package gov.mil.esp.pdf.rest.api;

import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import lombok.Data;

@Data
@JsonDeserialize
public class Request<T> {
	
	@Valid
	private T mensaje;

	@JsonCreator
	public Request(@JsonProperty T mensaje) {
		super();
		this.mensaje = mensaje;
	}

	public T getMensaje() {
		return mensaje;
	}

	public void setMensaje(T mensaje) {
		this.mensaje = mensaje;
	}

}
