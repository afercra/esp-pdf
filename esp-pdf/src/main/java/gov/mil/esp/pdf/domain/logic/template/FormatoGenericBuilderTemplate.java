package gov.mil.esp.pdf.domain.logic.template;

import java.io.IOException;
import java.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;

import gov.mil.esp.pdf.model.aspirante.DatosPersona;
import gov.mil.esp.pdf.model.formato.FormatoGenerico;
import gov.mil.esp.pdf.provider.file.FileBuilderImpl;
import gov.mil.esp.pdf.rest.client.PersonaClient;
import gov.mil.esp.pdf.rest.client.PersonaService;
import net.sf.jasperreports.engine.JRException;

@Component
public abstract class FormatoGenericBuilderTemplate {


	
	@Autowired
	PersonaService personaService;

	@Autowired
	FileBuilderImpl fileBuilderImpl;

	protected abstract FormatoGenerico prepareFormat(String documentoIdentidad);

	protected abstract String exportToPdfFile(FormatoGenerico formatoGenerico) throws IOException, JRException;

	protected DatosPersona getDatosPersona(String documentoIdentidad) {
		return personaService.getDatosPersona(documentoIdentidad);
	}

	protected String generateBase64(String nombreFormato, FormatoGenerico formatoGenerico)
			throws IOException, JRException {
		Gson gson = new Gson();
		/*return fileBuilderImpl.buildFormato(nombreFormato, gson.toJson(formatoGenerico));*/
		return Base64.getEncoder()
				.encodeToString(fileBuilderImpl.buildFormato(nombreFormato, gson.toJson(formatoGenerico)));

	}

}
