package gov.mil.esp.pdf;

import java.util.Arrays;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
@EnableFeignClients //(basePackages = "gov.mil.esp.pdf.rest.client")
//@ComponentScan(basePackages = "gov.mil.esp.pdf")
public class EspPdfApplication {

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(EspPdfApplication.class, args);
		
		Arrays.asList(context.getBeanDefinitionNames()).forEach(x -> System.out.println(x));;
		
		//JSONObject jSONObject = new JSONObject("{ \"primerNombre\":\"Ro\" }," + "" + "{ \"primerNombre\":\"aRo\" }");

		/*String rawObject = 
				"{ " + "\"pagina1\":{"
				+ " \"primerNombre\":\"Ro\","
				+ "\"primerApellido\":\"perez\""
				+ "}," 
				+ "\"pagina2\":{" 
						+ "\"barrioResidencia\":\"Barrio1\""
				+ "}," +
				 "\"pagina3\":{" 
						+ "\"primerApellidoHermano1\":\"Rojas Perez Juan Alejandro\""
				+ "}," 	
				+ "\"pagina4\":{" 
				+ "\"barrioResidencia\":\"Barrio1\""
				+ "}," 
				+ "\"pagina5\":{" 
				+ "\"barrioResidencia\":\"Barrio1\""
		      + "}," 
		      + "\"pagina6\":{" 
				+ "\"barrioResidencia\":\"Barrio1\""
		      + "}" +
				"}";*/
		/*
		Pagina1 pagina1 = new Pagina1();
		
		pagina1.setPrimerApellido("Rojas");
		pagina1.setSegundoApellido("Perez");
		pagina1.setPrimerNombre("Pedro")	;
		pagina1.setSegundoNombre("Juan");
		
		Pagina2 pagina2 = new Pagina2();
		pagina2.setBarrioResidencia("BarrioResidencia");
		
		Pagina3 pagina3 = new Pagina3();
		pagina3.setCiudadPaisEstudio1(null);
		
		Pagina4 pagina4 = new Pagina4();
		pagina4.setActividadEmpresa1(null);
		
		
		Pagina5 pagina5 = new Pagina5 ();
		pagina5.setAutoridadAntecedenteJudicial1("");
		
		Pagina6 pagina6 = new Pagina6();
		pagina6.setDependenciaRequiereEstudio("");
		
		//Pagina7 pagina7 = new Pagina7();
		//pagina7.setDependenciaRequiereEstudio("");
		
		FormatoActualizacionBase formatoActualizacionBase =  new FormatoActualizacionBase();
		
		formatoActualizacionBase.setPagina1(pagina1);
		formatoActualizacionBase.setPagina2(pagina2);
		formatoActualizacionBase.setPagina3(pagina3);
		formatoActualizacionBase.setPagina4(pagina4);
		formatoActualizacionBase.setPagina5(pagina5);
		formatoActualizacionBase.setPagina6(pagina6);
		
		Gson gson =  new Gson();
		System.out.println("GSON ------------------------------------->");
		System.out.println(gson.toJson(rawObject));
		
		*/
		
		
		//ApplicationContext context = SpringApplication.run(EspPdfApplication.class, args);

		//ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(gson.toJson(rawObject).getBytes());
	/*	try {
			System.out.println(rawObject);
			context.getBean(GeneradorFormatosEsp.class)
			.runJasper(gson.toJson(formatoActualizacionBase),"");
			System.out.println("Proccess finalized");
		} catch (BeansException | JRException| IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}
}
