package gov.mil.esp.pdf.domain.logic.command.ingreso.pagina;

import org.springframework.stereotype.Component;

import gov.mil.esp.pdf.domain.logic.command.GeneratePage;
import gov.mil.esp.pdf.model.aspirante.AntecedenteJudicial;
import gov.mil.esp.pdf.model.aspirante.DatosPersona;
import gov.mil.esp.pdf.model.formato.PaginaFormato;
import gov.mil.esp.pdf.model.formato.ingreso.Pagina6;

@Component
public class GeneratePaginaIngreso6 implements GeneratePage{

	@Override
	public PaginaFormato execute(DatosPersona datosPersona) {
		System.out.println("page6s");
		
		AntecedenteJudicial antecedenteJudicial1 = getAntecedenteJudicial(datosPersona, 0);
		AntecedenteJudicial antecedenteJudicial2 = getAntecedenteJudicial(datosPersona, 1);
		
		return Pagina6.builder()
				
				.razonesVinculacionInstitucion(datosPersona.getRazonDeBuscarVinculoConlaInstitucionYExpectativas())
			
				.responsableAntecedenteJudicial1SI((antecedenteJudicial1!=null)?getEsResponsableAntecedenteJudicialSi(antecedenteJudicial1.getEsReponsable()):"")
				.responsableAntecedenteJudicial1NO((antecedenteJudicial1!=null)?getEsResponsableAntecedenteJudicialNo(antecedenteJudicial1.getEsReponsable()):"")
				
				.fechaAntecedenteJudicial1((antecedenteJudicial1 != null )? antecedenteJudicial1.getFecha():"")
				.tipoAntecedenteJudicial1((antecedenteJudicial1 != null )? antecedenteJudicial1.getTipoInvestigacion():"")
				.causaAntecedenteJudicial1((antecedenteJudicial1 != null )? antecedenteJudicial1.getCausa():"")
				.estadoActualAntecedente1((antecedenteJudicial1 != null )? antecedenteJudicial1.getEstadoActualDelProceso():"")
				.autoridadAntecedenteJudicial1((antecedenteJudicial1 != null )? antecedenteJudicial1.getAutoridad():"")
				
				
				.responsableAntecedenteJudicial2SI((antecedenteJudicial2!=null)?getEsResponsableAntecedenteJudicialSi(antecedenteJudicial2.getEsReponsable()):"")
				.responsableAntecedenteJudicial2NO((antecedenteJudicial2!=null)?getEsResponsableAntecedenteJudicialNo(antecedenteJudicial1.getEsReponsable()):"")				
				
				.fechaAntecedenteJudicial2((antecedenteJudicial2 != null )? antecedenteJudicial2.getFecha():"")
				.tipoAntecedenteJudicial2((antecedenteJudicial2 != null )? antecedenteJudicial2.getTipoInvestigacion():"")
				.causaAntecedenteJudicial2((antecedenteJudicial2 != null )? antecedenteJudicial2.getCausa():"")
				.estadoActualAntecedente2((antecedenteJudicial2 != null )? antecedenteJudicial2.getEstadoActualDelProceso():"")
				.autoridadAntecedenteJudicial2((antecedenteJudicial2 != null )? antecedenteJudicial2.getAutoridad():"")

				.razonDeBuscarVinculoConlaInstitucionYExpectativas(datosPersona.getRazonDeBuscarVinculoConlaInstitucionYExpectativas())
				
				.datosAdicionales(datosPersona.getDatosAdicionales())
				.build();
	}
	
	
	private AntecedenteJudicial getAntecedenteJudicial(DatosPersona datosPersona, int indice) {
		if (datosPersona.getAntecedentesJudiciales() != null) {
			if (datosPersona.getAntecedentesJudiciales().size() > indice) {
				return datosPersona.getAntecedentesJudiciales().get(indice);
			}
		}
		return null;
	}
	
	private String getEsResponsableAntecedenteJudicialSi(String esResponsable) {
		if (esResponsable != null) {
			return (esResponsable.equalsIgnoreCase("si")) ? "X" : "";

		}
		return "";
	}

	private String getEsResponsableAntecedenteJudicialNo(String esResponsable) {
		if (esResponsable != null) {
			return (esResponsable.equalsIgnoreCase("no")) ? "X" : "";
		}

		return "";
	}
	
	
	
	
}