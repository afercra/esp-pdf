package gov.mil.esp.pdf.domain.logic.command.actualizacion.pagina;

import org.springframework.stereotype.Component;

import gov.mil.esp.pdf.domain.logic.command.GeneratePage;
import gov.mil.esp.pdf.model.aspirante.DatosPersona;
import gov.mil.esp.pdf.model.formato.PaginaFormato;
import gov.mil.esp.pdf.model.formato.actualizacion.Pagina5;

@Component
public class GeneratePaginaActualizacion5 implements GeneratePage{

		@Override
		public PaginaFormato execute(DatosPersona datosPersona) {
			
			return Pagina5.builder()
					/*.empresaRazonSocial1("").empresaRazonSocial2("").empresaRazonSocial3("")
					.actividadEmpresa1("").actividadEmpresa2("").actividadEmpresa3("")
					.tieneInformacionRiesgosaParaNacionSI("").opinionGruposMarginales("")
					.conoceArmasDeFuegoExplosivosCuandoYDonde("").conoceArmasDeFuegoExplosivosCualesHaManipulado("")
					.conoceArmasDeFuegoExplosivosCuales("").conoceArmasDeFuegoExplosivosNO("")
					.conoceArmasDeFuegoExplosivosSI("").motivoRetiroEntidadRelacionada1("").cargoEntidadRelacionada1("")
					.entidadRelacionada1("").motivoRetiroEntidadRelacionada2("").cargoEntidadRelacionada2("")
					.entidadRelacionada2("").motivoRetiroEntidadRelacionada3("").cargoEntidadRelacionada3("")
					.entidadRelacionada3("").motivoRetiroEntidadRelacionada4("").cargoEntidadRelacionada4("")
					.entidadRelacionada4("").descripcionActividadesViajeExterior("").tiempoPermanenciaViajeExterior2("")
					.tiempoPermanenciaViajeExterior4("").motivoViajeExterior4("").paisViajeExterior4("")
					.fechaViajeExterior4("").motivoViajeExterior3("").paisViajeExterior3("").fechaViajeExterior3("")
					.motivoViajeExterior2("").paisViajeExterior2("").fechaViajeExterior2("").motivoViajeExterior1("")
					.paisViajeExterior1("").fechaViajeExterior1("").actividadEconomicaEmpresa4("")
					.actividadEconomicaEmpresa3("").actividadEconomicaEmpresa2("").actividadEconomicaEmpresa1("")
					.tieneInformacionRiesgosaParaNacionNO("").recomendadoPor("").telefonoReferente3("")
					.direccionReferente3("").unidadReferente3("").nombreApellidosReferente3("").fuerzaReferente3("")
					.cargoReferente3("").gradoReferente3("").telefonoReferente2("").direccionReferente2("")
					.unidadReferente2("").nombreApellidosReferente2("").fuerzaReferente2("").cargoReferente2("")
					.gradoReferente2("").telefonoReferente("").direccionReferente("").unidadReferente1("")
					.nombreApellidosReferente1("").fuerzaReferente1("").cargoReferente1("").gradoReferente1("")
					.tieneInformacionActosCorruptosNO("").tieneInformacionActosCorruptosSI("")
					.conoceArmasDeFuegoExplosivosCuando("").tiempoPermanenciaViajeExterior1("")
					.tiempoPermanenciaViajeExterior3("").recomendadoPorAlguienDeLaInstitucionSI("")
					.recomendadoPorAlguienDeLaInstitucionNO("")*/

					.build();
		}
		
		
	}