package gov.mil.esp.pdf.domain.logic.command;

import gov.mil.esp.pdf.model.aspirante.DatosPersona;
import gov.mil.esp.pdf.model.formato.PaginaFormato;

public interface GeneratePage {
	
	PaginaFormato execute(DatosPersona datosPersona);
}
