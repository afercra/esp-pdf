package gov.mil.esp.pdf.domain.logic.ingreso;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import gov.mil.esp.pdf.domain.logic.PaginaFactory;
import gov.mil.esp.pdf.model.aspirante.DatosPersona;
import gov.mil.esp.pdf.model.formato.ingreso.FormatoIngresoBase;
import gov.mil.esp.pdf.model.formato.ingreso.Pagina1;
import gov.mil.esp.pdf.model.formato.ingreso.Pagina2;
import gov.mil.esp.pdf.model.formato.ingreso.Pagina3;
import gov.mil.esp.pdf.model.formato.ingreso.Pagina4;
import gov.mil.esp.pdf.model.formato.ingreso.Pagina5;
import gov.mil.esp.pdf.model.formato.ingreso.Pagina6;
import gov.mil.esp.pdf.model.formato.ingreso.Pagina7;

@Component
public class FormatoIngresoBuilder {

	private FormatoIngresoBase formatoIngresoBase;
	private DatosPersona datosPersona;

	@Autowired
	private PaginaFactory paginaFactory;

	public FormatoIngresoBuilder(DatosPersona datosPersona) {
		this.datosPersona = datosPersona;
	}

	public FormatoIngresoBase buildFormatoIngresoBase(DatosPersona datosPersona) {
		return FormatoIngresoBase.builder()
				.pagina1(buildPagina1(datosPersona))
				.pagina2(buildPagina2(datosPersona))
				.pagina3(buildPagina3(datosPersona))
				.pagina4(buildPagina4(datosPersona))
				.pagina5(buildPagina5(datosPersona))
				.pagina6(buildPagina6(datosPersona))
				.pagina7(buildPagina7(datosPersona))
				.build();
	}

	Pagina1 buildPagina1(DatosPersona datosPersona) {
		return (Pagina1) paginaFactory.generarPagina("generatePaginaIngreso1").execute(datosPersona);
	}

	Pagina2 buildPagina2(DatosPersona datosPersona) {
		System.out.println("on formatoIngresoBuilder");
		System.out.println(paginaFactory.generarPagina("generatePaginaIngreso2").execute(datosPersona));
		return (Pagina2) paginaFactory.generarPagina("generatePaginaIngreso2").execute(datosPersona);
	}

	Pagina3 buildPagina3(DatosPersona datosPersona) {
		return (Pagina3) paginaFactory.generarPagina("generatePaginaIngreso3").execute(datosPersona);
	}

	Pagina4 buildPagina4(DatosPersona datosPersona) {
		return (Pagina4) paginaFactory.generarPagina("generatePaginaIngreso4").execute(datosPersona);
	}

	Pagina5 buildPagina5(DatosPersona datosPersona) {
		return (Pagina5) paginaFactory.generarPagina("generatePaginaIngreso5").execute(datosPersona);

	}

	Pagina6 buildPagina6(DatosPersona datosPersona) {
		return (Pagina6) paginaFactory.generarPagina("generatePaginaIngreso6").execute(datosPersona);
		
	}

	Pagina7 buildPagina7(DatosPersona datosPersona) {
		return (Pagina7) paginaFactory.generarPagina("generatePaginaIngreso7").execute(datosPersona);
		
	}



}
