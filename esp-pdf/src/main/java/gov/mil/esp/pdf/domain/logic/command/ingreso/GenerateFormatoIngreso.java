package gov.mil.esp.pdf.domain.logic.command.ingreso;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import gov.mil.esp.pdf.domain.logic.command.GenerateFormato;
import gov.mil.esp.pdf.domain.logic.ingreso.FormatoIngresoBuilder;
import gov.mil.esp.pdf.domain.logic.template.FormatoGenericBuilderTemplate;
import gov.mil.esp.pdf.model.aspirante.DatosPersona;
import gov.mil.esp.pdf.model.formato.FormatoGenerico;
import gov.mil.esp.pdf.model.formato.ingreso.FormatoIngresoBase;
import net.sf.jasperreports.engine.JRException;

@Component
public class GenerateFormatoIngreso extends FormatoGenericBuilderTemplate implements GenerateFormato {

	
	private FormatoIngresoBase formatoIngresoBase;
	
	
	private String documentoIdentidad;
	private final String formatName = "formatoIngreso.jasper";
	
	@Autowired
	private FormatoIngresoBuilder formatoIngresoBuilder;


	@Autowired
	public GenerateFormatoIngreso() {
	}
	
	public GenerateFormatoIngreso(String documentoIdentidad) {
		this.documentoIdentidad = documentoIdentidad;
	}
	
	public void setDocumentoIdentidad(String documentoIdentidad) {
		this.documentoIdentidad = documentoIdentidad;
	}
	
	public DatosPersona getDatosPersona() {
		return this.getDatosPersona(documentoIdentidad);
	}
	
	protected FormatoGenerico prepareFormat(String documentoIdentidad) {		
		FormatoGenerico formatoGenerico =formatoIngresoBuilder.buildFormatoIngresoBase(getDatosPersona());
		System.out.println("on generateformatoingreso line 48");
		System.out.println(formatoGenerico);
		return formatoGenerico;
	}
		
	@Override
	protected String exportToPdfFile(FormatoGenerico formatoGenerico) throws IOException, JRException {		
		return this.generateBase64(formatName, formatoGenerico);
	}


	@Override
	public String execute() {
		try {
			System.out.println("begfore exportTOPdfFIle");
			return exportToPdfFile(prepareFormat(documentoIdentidad));
		} catch (IOException | JRException e) {
			System.out.println("error Getting file ---------------->>");
			e.printStackTrace();
		}
		return null;
	}



}
