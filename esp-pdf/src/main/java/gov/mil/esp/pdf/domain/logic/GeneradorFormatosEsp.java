package gov.mil.esp.pdf.domain.logic;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JsonDataSource;
import net.sf.jasperreports.engine.util.JRLoader;

@Component
public class GeneradorFormatosEsp {

	@Autowired
	Environment env;

	Logger logger = LoggerFactory.getLogger(GeneradorFormatosEsp.class);

	public void generarPdf(String json) throws IOException {

	}

	public String runJasper(String json, String reportTemplate)
			throws JRException, IOException {
		Resource resource = new ClassPathResource("formatoActualizacion.jasper");
		InputStream jasperStream = resource.getInputStream();
		JasperReport jasperReport = (JasperReport) JRLoader.loadObject(jasperStream);
		Map<String, Object> params = new HashMap<>();
		JsonDataSource jds = new JsonDataSource(new ByteArrayInputStream(json.getBytes(StandardCharsets.UTF_8)));
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, jds);
		
		String outputDir = env.getRequiredProperty("pdf.output.single");
		FileUtils.forceMkdir(new File(outputDir));
		String filePath = "Pathtofile.pdf";
		File file = new File(outputDir + File.separator + filePath);
		// System.out.println("before file exists");
		if (file.exists())
			file.delete();
		//OutputStream outStream = new FileOutputStream(file);
		JasperExportManager.exportReportToPdf(jasperPrint);
		
		// JasperExportManager.exportReportToPdfFile(filePath);
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(file));

		// file
		// outStream.write(1);
		//outStream.close();
		System.out.println("after close ");
		return filePath;

	}

}
