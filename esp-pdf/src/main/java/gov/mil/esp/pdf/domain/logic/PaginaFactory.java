
package gov.mil.esp.pdf.domain.logic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import gov.mil.esp.pdf.domain.logic.command.GeneratePage;

@Component
public class PaginaFactory {
	
	private ApplicationContext context;
	
	@Autowired
	public PaginaFactory(ApplicationContext  context) {
		this.context = context;
	}

	public GeneratePage generarPagina (String paginaAgenerar) {		
		return (GeneratePage) context.getBean(paginaAgenerar);
	}
  
}
