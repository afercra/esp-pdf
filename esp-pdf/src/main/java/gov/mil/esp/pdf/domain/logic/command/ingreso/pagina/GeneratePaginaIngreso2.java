package gov.mil.esp.pdf.domain.logic.command.ingreso.pagina;

import org.springframework.stereotype.Component;

import gov.mil.esp.pdf.domain.logic.command.GeneratePage;
import gov.mil.esp.pdf.model.aspirante.DatosPersona;
import gov.mil.esp.pdf.model.aspirante.Hijo;
import gov.mil.esp.pdf.model.formato.PaginaFormato;
import gov.mil.esp.pdf.model.formato.ingreso.Pagina2;

@Component
public class GeneratePaginaIngreso2 implements GeneratePage {
	


	@Override
	public PaginaFormato execute(DatosPersona datosPersona) {
		System.out.println("page2");
		Hijo hijo1 = getHijo(datosPersona,0);
		Hijo hijo2 = getHijo(datosPersona,1);
		Hijo hijo3 = getHijo(datosPersona,2);
		Hijo hijo4 = getHijo(datosPersona,3);
		
		
		System.out.println("on generadorpagina 2  ------->");
		System.out.println(datosPersona.getBarrioResidenciaActual());
		return Pagina2.builder()
				.barrioResidencia(datosPersona.getBarrioResidenciaActual())
				.telefonoCelularResidencia(datosPersona.getNumeroCelularResidenciaActual())
				.telefonoFijoResidencia(datosPersona.getNumeroTelefonoResidenciaActual())
				.ciudadYdepartamentoResidencia(datosPersona.getCiudadResidenciaActual() + " - "+datosPersona.getDepartamentoResidenciaActual())
				.correosElectronicosResidencia(datosPersona.getCorreoElectronico())
				.redesSocialesResidencia(datosPersona.getRedesSociales())
				.residenciaAnteriorDesde(datosPersona.getResidenciaAnteriorDesde())
				.residenciaAnteriorHasta(datosPersona.getResidenciaAnteriorHasta())
				.residenciaAnteriorDireccion(datosPersona.getDireccionResidenciaAnterior())
				.telefonosResidenciaAnterior(datosPersona.getNumeroTelefonoResidenciaAnterior())
				.ciudadPaisResidenciaAnterior(datosPersona.getCiudadResidenciaAnterior()+"-"+datosPersona.getDepartamentoResidenciaAnterior()+"("+datosPersona.getPaisResidenciaAnterior()+")")				
				.nombreEsposa(datosPersona.getNombreCompletoCompaneroSentimental())
				.profesionEsposa(datosPersona.getProfesionUOficionCompaneroSentimental())
				.numeroDocumentoEsposa(datosPersona.getNumeroDocumentoCompaneroSentimental())
				.direccionEsposa(datosPersona.getDireccionCompaneroSentimental())
				.celularYTelefonoEsposa(datosPersona.getNumeroCelularCompaneroSentimental())
				.correoElectronicoEsposa(datosPersona.getCorreoElectronico())
				.redesSocialesEsposa(datosPersona.getRedSocialConyugue())
				
				
				.nombreHijo1((hijo1 !=null)?hijo1.getNombreCompleto():"")
				.nombreHijo2((hijo2 !=null)?hijo2.getNombreCompleto():"")
				.nombreHijo3((hijo3 !=null)?hijo3.getNombreCompleto():"")
				.nombreHijo4((hijo4 !=null)?hijo4.getNombreCompleto():"")
				.numeroIdentificacionHijo1((hijo1 !=null)?hijo1.getNumeroDocumento():"")
				.numeroIdentificacionHijo2((hijo2 !=null)?hijo2.getNumeroDocumento():"")
				.numeroIdentificacionHijo3((hijo3 !=null)?hijo3.getNumeroDocumento():"")
				.numeroIdentificacionHijo4((hijo4 !=null)?hijo4.getNumeroDocumento():"")
				.edadHijo1((hijo1 !=null)?hijo1.getEdad():"")
				.edadHijo2((hijo2 !=null)?hijo2.getEdad():"")
				.edadHijo3((hijo3 !=null)?hijo3.getEdad():"")
				.edadHijo4((hijo4 !=null)?hijo4.getEdad():"")
				
				.nombrePadre(getNombrePadre(datosPersona))
				
				
				.padreViveSi(padreViveSi(datosPersona.getVivePadre()))
			   	.padreViveNo(padreViveNo(datosPersona.getVivePadre()))
				.numeroDocumentoPadre(datosPersona.getNumeroDocumentoPadre())
				.telefonosPadre(datosPersona.getNumeroTelefonoPadre())
				.direccionPadre(datosPersona.getDireccionPadre())
				.profesionPadre(datosPersona.getOcupacionPadre())
				.nombreMadre(getNombreMadre(datosPersona))
				.madreViveSi(madreViveSi(datosPersona.getViveMadre()))
				.madreViveNo(madreViveNo(datosPersona.getViveMadre()))
				.numeroDocumentoMadre(datosPersona.getNumeroDocumentoMadre())
				.telefonosMadre(datosPersona.getNumeroTelefonoMadre())
				.direccionMadre(datosPersona.getDireccionMadre())
				.profesionMadre(datosPersona.getOcupacionMadre())
				.profesionUOficio(datosPersona.getProfesion())
				.build();
				
				

	}
	
	public String madreViveSi(String madreVive) {
		return (madreVive.equalsIgnoreCase("si"))
				?"X":"";
	}

	
	public String madreViveNo(String padreVive) {
		return (padreVive.equalsIgnoreCase("no"))
				?"X":"";
	}	
	
	public String padreViveSi(String padreVive) {
		return (padreVive.equalsIgnoreCase("si"))
				?"X":"";
	}
	
	public String padreViveNo(String padreVive) {
		return (padreVive.equalsIgnoreCase("no"))
				?"X":"";
	}
	
	
	public String getNombrePadre(DatosPersona datosPersona) {
		return datosPersona.getPrimerNombrePadre()
		+" "+datosPersona.getSegundoNombrePadre()
		+" "+datosPersona.getPrimerApellidoPadre()
		+" "+datosPersona.getSegundoApellidoPadre();
	}
	
	public String getNombreMadre(DatosPersona datosPersona) {
		return datosPersona.getPrimerNombreMadre()
		+" "+datosPersona.getSegundoNombreMadre()
		+" "+datosPersona.getPrimerApellidoMadre()
		+" "+datosPersona.getSegundoApellidoMadre();
	}
	
	
	public Hijo getHijo(DatosPersona datosPersona,int posicion) {
		if(datosPersona.getHijos()!=null) {
		if(datosPersona.getHijos().size()>posicion) {
			return datosPersona.getHijos().get(posicion);
		}
		}
		return null;
	}
	

	
	public String getNombreCompletoConyugue(DatosPersona datosPersona) {
		return datosPersona.getPrimerApellidoCompaneroSentimental()+" "+datosPersona.getSegundoApellidoCompaneroSentimental()
		+" "+ datosPersona.getPrimerNombreCompaneroSentimental()+" "+datosPersona.getSegundoNombreCompaneroSentimental();
	}
	
}