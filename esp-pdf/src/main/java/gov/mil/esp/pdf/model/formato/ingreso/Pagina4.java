package gov.mil.esp.pdf.model.formato.ingreso;

import gov.mil.esp.pdf.model.formato.PaginaFormato;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Pagina4 extends PaginaFormato{
	
	
	
	private String nombresApellidosReferencia1; 
	private String ocupacionReferencia1; 
	private String direccionReferencia1; 
	private String telefonosReferencia1; 
	private String nombresApellidosReferencia2; 
	private String ocupacionReferencia2; 
	private String direccionReferencia2; 
	private String telefonosReferencia2; 
	
	
	private String organismoConocidoEntidadPublica2; 
	private String unidadConocidoEntidadPublica2; 
	private String telefonosConocidoEntidadPublica2; 
	private String nombresConocidoEntidadPublica2; 
	private String direccionConocidoEntidadPublica2; 
	private String gradoConocidoEntidadPublica2; 
	private String telefonosConocidoEntidadPublica1; 
	private String unidadConocidoEntidadPublica1; 
	private String organismoConocidoEntidadPublica1; 
	private String direccionConocidoEntidadPublica1; 
	private String nombresConocidoEntidadPublica1; 
	private String gradoConocidoEntidadPublica1; 
	
	
	private String otrosIngresos; 
	private String honorarios; 
	private String arriendos;
	private String salariosDemasIngresos; 
	private String cesantiaseintereses; 
	

	private String saldoCuenta1; 
	private String numeroCuenta1; 
	private String tipoCuenta1; 
	private String sedeFinancieraCuenta1; 
	private String entidadFinancieraCuenta1; 
	private String entidadFinancieraCuenta2; 
	private String sedeFinancieraCuenta2; 
	private String tipoCuenta2; 
	private String numeroCuenta2; 
	private String saldoCuenta2; 
	private String entidadFinancieraCuenta3; 
	private String sedeFinancieraCuenta3; 
	private String tipoCuenta3; 
	private String numeroCuenta3; 
	private String saldoCuenta3; 
	
	
	private String tipoBien1; 
	private String identificacionBien1; 
	private String valorBien1; 
	private String tipoBien2; 
	private String identificacionBien2; 
	private String valorBien2; 
	private String tipoBien3; 
	private String identificacionBien3; 
	private String valorBien3; 
	private String tipoBien4; 
	private String identificacionBien4; 
	private String valorBien4; 
	
	
	private String entidadObligacionCrediticia1; 
	private String valorObligacionCrediticia1; 
	private String conceptoObligacionCrediticia1; 
	private String entidadObligacionCrediticia2; 
	private String valorObligacionCrediticia2; 
	private String conceptoObligacionCrediticia2; 
	private String entidadObligacionCrediticia3; 
	private String valorObligacionCrediticia3; 
	private String conceptoObligacionCrediticia3; 
	private String entidadObligacionCrediticia4; 
	private String valorObligacionCrediticia4; 
	private String conceptoObligacionCrediticia4; 
	
	
	private String nombreOrganizacion1; 
	private String calidadMiembroOrganizacion1; 
	private String nombreOrganizacion2; 
	private String calidadMiembroOrganizacion2; 
	private String nombreOrganizacion3; 
	private String calidadMiembroOrganizacion3;
	private String total;
	

	
}
