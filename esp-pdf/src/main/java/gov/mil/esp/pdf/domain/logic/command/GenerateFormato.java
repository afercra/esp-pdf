package gov.mil.esp.pdf.domain.logic.command;


public interface GenerateFormato {
	
	String execute();

}
