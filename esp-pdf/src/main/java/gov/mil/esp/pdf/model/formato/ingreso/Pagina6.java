package gov.mil.esp.pdf.model.formato.ingreso;

import gov.mil.esp.pdf.model.formato.PaginaFormato;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Pagina6 extends PaginaFormato {

	private String razonesVinculacionInstitucion;
	
	private String fechaAntecedenteJudicial1;
	private String tipoAntecedenteJudicial1;
	private String causaAntecedenteJudicial1;
	private String estadoActualAntecedente1;
	private String responsableAntecedenteJudicial1SI;
	private String responsableAntecedenteJudicial1NO;
	private String fechaAntecedenteJudicial2;
	private String tipoAntecedenteJudicial2;
	private String causaAntecedenteJudicial2;
	private String estadoActualAntecedente2;
	private String responsableAntecedenteJudicial2SI;
	private String responsableAntecedenteJudicial2NO;
	private String autoridadAntecedenteJudicial1;
	private String autoridadAntecedenteJudicial2;
	private String datosAdicionales;
	private String razonDeBuscarVinculoConlaInstitucionYExpectativas;

}
