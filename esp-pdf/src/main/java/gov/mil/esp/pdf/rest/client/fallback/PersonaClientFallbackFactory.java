package gov.mil.esp.pdf.rest.client.fallback;

import org.springframework.stereotype.Component;

import feign.hystrix.FallbackFactory;
import gov.mil.esp.pdf.rest.client.PersonaClient;

@Component
public class PersonaClientFallbackFactory implements FallbackFactory<PersonaClient>{

	@Override
	public PersonaClient create(Throwable cause) {
		System.out.println("in fallback --->");
		cause.printStackTrace();
		return new PersonaClientFallback(cause);
	}

	
}
