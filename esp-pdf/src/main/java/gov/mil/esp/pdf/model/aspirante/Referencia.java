package gov.mil.esp.pdf.model.aspirante;

import lombok.Data;

@Data
public class Referencia {
	
	String primerApellido;
	String segundoApellido;
	String segundoNombre;
	String primerNombre;
	String nombreCompleto;
	String tipoDocumento;
	String numeroDocumento;
	String profesionUOficio;
	String direccion;
	String departamento;
	String ciudad;
	String numeroCelular;
	String numeroTelefono;
	String fuerza;
	String unidad;
	String cargo;
	String grado;
	String organismoEntidad;
	
}
