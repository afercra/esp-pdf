package gov.mil.esp.pdf.domain.logic.command.ingreso.pagina;

import org.springframework.stereotype.Component;

import gov.mil.esp.pdf.domain.logic.command.GeneratePage;
import gov.mil.esp.pdf.model.aspirante.BienPatrimonial;
import gov.mil.esp.pdf.model.aspirante.Cuenta;
import gov.mil.esp.pdf.model.aspirante.DatosPersona;
import gov.mil.esp.pdf.model.aspirante.ObligacionCrediticia;
import gov.mil.esp.pdf.model.aspirante.OtroIngreso;
import gov.mil.esp.pdf.model.aspirante.ParticipacionOrganizacion;
import gov.mil.esp.pdf.model.aspirante.Referencia;
import gov.mil.esp.pdf.model.formato.PaginaFormato;
import gov.mil.esp.pdf.model.formato.ingreso.Pagina4;

@Component
public class GeneratePaginaIngreso4 implements GeneratePage{

	@Override
	public PaginaFormato execute(DatosPersona datosPersona) {
		
		Referencia referencia1 = getReferenciaNoFamiliar(datosPersona, 0);
		Referencia referencia2 = getReferenciaNoFamiliar(datosPersona, 1);
		
		Referencia conocidoEntidadPublica1 = getConocidoEntidadPublica(datosPersona, 0);
		Referencia conocidoEntidadPublica2 = getConocidoEntidadPublica(datosPersona, 1);
		
		
		Cuenta cuenta1 = getCuenta(datosPersona, 0);
		Cuenta cuenta2 = getCuenta(datosPersona, 1);
		Cuenta cuenta3 = getCuenta(datosPersona, 2);
		Cuenta cuenta4 = getCuenta(datosPersona, 3);
		
		
		BienPatrimonial bienPatrimonial1 = getBienPatrimonial(datosPersona, 0);
		BienPatrimonial bienPatrimonial2 = getBienPatrimonial(datosPersona, 1);
		BienPatrimonial bienPatrimonial3 = getBienPatrimonial(datosPersona, 2);
		BienPatrimonial bienPatrimonial4 = getBienPatrimonial(datosPersona, 3);
		
		ObligacionCrediticia obligacionCrediticia1 = getObligacionCrediticia(datosPersona, 0);
		ObligacionCrediticia obligacionCrediticia2 = getObligacionCrediticia(datosPersona, 1);
		ObligacionCrediticia obligacionCrediticia3 = getObligacionCrediticia(datosPersona, 2);
		ObligacionCrediticia obligacionCrediticia4 = getObligacionCrediticia(datosPersona, 3);
		
		ParticipacionOrganizacion participacionOrganizacion1 = getParticipacionOrganizacion(datosPersona, 0);
		ParticipacionOrganizacion participacionOrganizacion2 = getParticipacionOrganizacion(datosPersona, 1);
		ParticipacionOrganizacion participacionOrganizacion3= getParticipacionOrganizacion(datosPersona, 2);
		
		System.out.println("page4");
		
		
		return Pagina4.builder()
				
				
				
				.nombresApellidosReferencia1((referencia1 != null)?getNombreCompletoReferencia(referencia1):"")
				.ocupacionReferencia1((referencia1 != null)?referencia1.getProfesionUOficio():"")
				.direccionReferencia1((referencia1 != null)?referencia1.getDireccion():"")
				.telefonosReferencia1((referencia1 != null)?referencia1.getNumeroTelefono():"")
				
				.nombresApellidosReferencia2((referencia2 != null)?getNombreCompletoReferencia(referencia2):"")
				.ocupacionReferencia2((referencia2 != null)?referencia2.getProfesionUOficio():"")
				.direccionReferencia2((referencia2 != null)?referencia2.getDireccion():"")
				.telefonosReferencia2((referencia2 != null)?referencia2.getNumeroTelefono():"")
				
				
				.organismoConocidoEntidadPublica1((conocidoEntidadPublica1!=null)?conocidoEntidadPublica1.getOrganismoEntidad():"")
				.unidadConocidoEntidadPublica1((conocidoEntidadPublica1!=null)?conocidoEntidadPublica1.getUnidad():"")
				.telefonosConocidoEntidadPublica1((conocidoEntidadPublica1!=null)?conocidoEntidadPublica1.getNumeroTelefono():"")
				.nombresConocidoEntidadPublica1((conocidoEntidadPublica1!=null)?getNombreCompletoReferencia(conocidoEntidadPublica1):"")
				.direccionConocidoEntidadPublica1((conocidoEntidadPublica1!=null)?conocidoEntidadPublica1.getDireccion():"")
				.gradoConocidoEntidadPublica1((conocidoEntidadPublica1!=null)?conocidoEntidadPublica1.getGrado():"")
				

				
				.organismoConocidoEntidadPublica2((conocidoEntidadPublica2!=null)?conocidoEntidadPublica2.getOrganismoEntidad():"")
				.unidadConocidoEntidadPublica2((conocidoEntidadPublica2!=null)?conocidoEntidadPublica1.getUnidad():"")
				.telefonosConocidoEntidadPublica2((conocidoEntidadPublica2!=null)?conocidoEntidadPublica2.getNumeroTelefono():"")
				.nombresConocidoEntidadPublica2((conocidoEntidadPublica2!=null)?getNombreCompletoReferencia(conocidoEntidadPublica2):"")
				.direccionConocidoEntidadPublica2((conocidoEntidadPublica2!=null)?conocidoEntidadPublica2.getDireccion():"")
				.gradoConocidoEntidadPublica2((conocidoEntidadPublica2!=null)?conocidoEntidadPublica2.getGrado():"")

				
				.honorarios(datosPersona.getHonorarios())
				.arriendos(datosPersona.getArriendos())
				.salariosDemasIngresos(datosPersona.getSalarioIngresosLaborales())
				.cesantiaseintereses(datosPersona.getCesantiasIntereses())
				.otrosIngresos(getOtrosIngresos(datosPersona))
				.total("")
				.saldoCuenta1((cuenta1!=null)?cuenta1.getSaldoCuenta():"")
				.numeroCuenta1((cuenta1!=null)?cuenta1.getNumeroCuenta():"")
				.tipoCuenta1((cuenta1!=null)?cuenta1.getTipoCuenta():"")
				.sedeFinancieraCuenta1((cuenta1!=null)?cuenta1.getSedeCuenta():"")
				.entidadFinancieraCuenta1((cuenta1!=null)?cuenta1.getEntidadFinanciera():"")
				
				.saldoCuenta2((cuenta2!=null)?cuenta2.getSaldoCuenta():"")
				.numeroCuenta2((cuenta2!=null)?cuenta2.getNumeroCuenta():"")
				.tipoCuenta2((cuenta2!=null)?cuenta2.getTipoCuenta():"")
				.sedeFinancieraCuenta2((cuenta2!=null)?cuenta2.getSedeCuenta():"")
				.entidadFinancieraCuenta2((cuenta2!=null)?cuenta2.getEntidadFinanciera():"")
				
				.saldoCuenta3((cuenta3!=null)?cuenta3.getSaldoCuenta():"")
				.numeroCuenta3((cuenta3!=null)?cuenta3.getNumeroCuenta():"")
				.tipoCuenta3((cuenta3!=null)?cuenta3.getTipoCuenta():"")
				.sedeFinancieraCuenta3((cuenta3!=null)?cuenta3.getSedeCuenta():"")
				.entidadFinancieraCuenta3((cuenta3!=null)?cuenta3.getEntidadFinanciera():"")
				
				.tipoBien1((bienPatrimonial1 != null)?bienPatrimonial1.getTipo():"")
				.identificacionBien1((bienPatrimonial1 != null)?bienPatrimonial1.getIdentificacion():"")
				.valorBien1((bienPatrimonial1 != null)?bienPatrimonial1.getValor():"")
				
				.tipoBien2((bienPatrimonial2 != null)?bienPatrimonial2.getTipo():"")
				.identificacionBien2((bienPatrimonial2 != null)?bienPatrimonial2.getIdentificacion():"")
				.valorBien2((bienPatrimonial2 != null)?bienPatrimonial2.getValor():"")
				
				.tipoBien3((bienPatrimonial3 != null)?bienPatrimonial3.getTipo():"")
				.identificacionBien3((bienPatrimonial3 != null)?bienPatrimonial3.getIdentificacion():"")
				.valorBien3((bienPatrimonial3 != null)?bienPatrimonial3.getValor():"")
				
				.tipoBien4((bienPatrimonial4 != null)?bienPatrimonial4.getTipo():"")
				.identificacionBien4((bienPatrimonial4 != null)?bienPatrimonial4.getIdentificacion():"")
				.valorBien4((bienPatrimonial4 != null)?bienPatrimonial4.getValor():"")
				
				.entidadObligacionCrediticia1((obligacionCrediticia1 != null)? obligacionCrediticia1.getEntidad():"")
				.valorObligacionCrediticia1((obligacionCrediticia1 != null)? obligacionCrediticia1.getValor():"")
				.conceptoObligacionCrediticia1((obligacionCrediticia1 != null)? obligacionCrediticia1.getConcepto():"")
				
				.entidadObligacionCrediticia2((obligacionCrediticia2 != null)? obligacionCrediticia2.getEntidad():"")
				.valorObligacionCrediticia2((obligacionCrediticia2 != null)? obligacionCrediticia2.getValor():"")
				.conceptoObligacionCrediticia2((obligacionCrediticia2 != null)? obligacionCrediticia2.getConcepto():"")
				
				.entidadObligacionCrediticia3((obligacionCrediticia3 != null)? obligacionCrediticia3.getEntidad():"")
				.valorObligacionCrediticia3((obligacionCrediticia3 != null)? obligacionCrediticia3.getValor():"")
				.conceptoObligacionCrediticia3((obligacionCrediticia3 != null)? obligacionCrediticia3.getConcepto():"")
				
				.entidadObligacionCrediticia4((obligacionCrediticia4 != null)? obligacionCrediticia4.getEntidad():"")
				.valorObligacionCrediticia4((obligacionCrediticia4 != null)? obligacionCrediticia4.getValor():"")
				.conceptoObligacionCrediticia4((obligacionCrediticia4 != null)? obligacionCrediticia4.getConcepto():"")
				
				.nombreOrganizacion1((participacionOrganizacion1 != null)?participacionOrganizacion1.getNombre():"")
				.calidadMiembroOrganizacion1((participacionOrganizacion1 != null)?participacionOrganizacion1.getCalidadMiembro():"")
				
				.nombreOrganizacion2((participacionOrganizacion2 != null)?participacionOrganizacion2.getNombre():"")
				.calidadMiembroOrganizacion2((participacionOrganizacion2 != null)?participacionOrganizacion2.getCalidadMiembro():"")
				
				.nombreOrganizacion3((participacionOrganizacion3 != null)?participacionOrganizacion3.getNombre():"")
				.calidadMiembroOrganizacion3((participacionOrganizacion3 != null)?participacionOrganizacion3.getCalidadMiembro():"")



				.build();
	}
	
	
	private String getOtrosIngresos(DatosPersona datosPersona) {
		
		StringBuilder sb = new StringBuilder();
		datosPersona.getOtrosIngresos().forEach(x->{
			sb.append(x.getConcepto()+":"+x.getValor()+","); 
		});
		
		return sb.toString();
	}
	
	
	 private Referencia getReferenciaNoFamiliar(DatosPersona datosPersona, int posicion) {

				if(datosPersona.getReferenciasNoFamiliares()!=null)
				 {
					if(datosPersona.getReferenciasNoFamiliares().size() > posicion)
						return datosPersona.getReferenciasNoFamiliares().get(posicion);
				 }
				
				return null;
		 
	 }
	 
	 
	 private Referencia getConocidoEntidadPublica(DatosPersona datosPersona, int posicion) {

			if(datosPersona.getParientesOAmigosEnLaInstitucion()!=null)
			 {
				if(datosPersona.getParientesOAmigosEnLaInstitucion().size() > posicion)
					return datosPersona.getParientesOAmigosEnLaInstitucion().get(posicion);
			 }
			
			return null;
	 
    }
	 
	 private Cuenta getCuenta(DatosPersona datosPersona, int posicion) {

			if(datosPersona.getCuentasDinero()!=null)
			 {
				if(datosPersona.getCuentasDinero().size() > posicion)
					return datosPersona.getCuentasDinero().get(posicion);
			 }
			
			return null;
	 
     }
	 
	 
	 private BienPatrimonial getBienPatrimonial(DatosPersona datosPersona, int posicion) {
			if(datosPersona.getBienesPatrimoniales()!=null)
			 {
				if(datosPersona.getBienesPatrimoniales().size() > posicion)
					return datosPersona.getBienesPatrimoniales().get(posicion);
			 }
			
			return null;
	 }
	 
	 private ObligacionCrediticia getObligacionCrediticia(DatosPersona datosPersona, int posicion) {
			if(datosPersona.getParientesOAmigosEnLaInstitucion()!=null)
			 {
				if(datosPersona.getObligacionesCrediticias().size() > posicion)
					return datosPersona.getObligacionesCrediticias().get(posicion);
			 }
			
			return null;
	 }
	 
	 private ParticipacionOrganizacion getParticipacionOrganizacion(DatosPersona datosPersona, int posicion) {
			if(datosPersona.getParticipacionOrganizaciones()!=null)
			 {
				if(datosPersona.getParticipacionOrganizaciones().size() > posicion)
					return datosPersona.getParticipacionOrganizaciones().get(posicion);
			 }
			
			return null;
	 }
	
	 
	 private String getNombreCompletoReferencia(Referencia referencia) {
		 return referencia.getPrimerNombre()+" "+referencia.getSegundoNombre()
		       +" "+referencia.getPrimerApellido()+" "+referencia.getSegundoApellido();
	 }
	 
}