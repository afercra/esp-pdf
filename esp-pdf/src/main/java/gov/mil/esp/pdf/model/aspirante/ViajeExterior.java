package gov.mil.esp.pdf.model.aspirante;

import lombok.Data;

@Data
public class ViajeExterior {
	
	String fecha;
	String paisVisitado;
	String motivo;
	String tiempoPermanencia;
}
