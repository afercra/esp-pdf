package gov.mil.esp.pdf.model.formato.ingreso;

import gov.mil.esp.pdf.model.formato.PaginaFormato;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Pagina5 extends PaginaFormato {

	private String empresaRazonSocial1; 
	private String empresaRazonSocial2; 
	private String empresaRazonSocial3; 
	private String actividadEmpresa1; 
	private String actividadEmpresa2; 
	private String actividadEmpresa3;
	
	
	private String descripcionActividadesViajeExterior; 
	private String tiempoPermanenciaViajeExterior2; 
	private String tiempoPermanenciaViajeExterior4; 
	private String motivoViajeExterior4; 
	private String paisViajeExterior4; 
	private String fechaViajeExterior4; 
	private String motivoViajeExterior3; 
	private String paisViajeExterior3; 
	private String fechaViajeExterior3; 
	private String motivoViajeExterior2; 
	private String paisViajeExterior2; 
	private String fechaViajeExterior2; 
	private String motivoViajeExterior1; 
	private String paisViajeExterior1; 
	private String fechaViajeExterior1;
	private String tiempoPermanenciaViajeExterior1; 
	private String tiempoPermanenciaViajeExterior3; 
	
	private String motivoRetiroEntidadRelacionada1;
	private String cargoEntidadRelacionada1; 
	private String entidadRelacionada1; 
	private String motivoRetiroEntidadRelacionada2; 
	private String cargoEntidadRelacionada2;
	private String entidadRelacionada2;
	
 
	private String conoceArmasDeFuegoExplosivosSI;
	private String conoceArmasDeFuegoExplosivosNO; 
	 
	private String conoceArmasDeFuegoExplosivosCuando;
	private String conoceArmasDeFuegoExplosivosCuandoYDonde; 
	private String conoceArmasDeFuegoExplosivosCualesHaManipulado; 
	private String conoceArmasDeFuegoExplosivosCuales;
	private String armasManipuladasEnCompaniaDe;
	private String opinionGruposMarginales;
	
	private String tieneInformacionRiesgosaParaNacionSI; 
	private String tieneInformacionRiesgosaParaNacionNO;
	private String informacionRiesgosaParaNacion;
	
	private String tieneInformacionActosCorruptosNO; 
	private String tieneInformacionActosCorruptosSI;
	private String informacionActosCorrupcionEnInstitucion;
	
	private String recomendadoPorAlguienDeLaInstitucionSI; 
	private String recomendadoPorAlguienDeLaInstitucionNO; 
	private String recomendadoPor; 
	
	private String telefonoReferente; 
	private String direccionReferente; 

	
	private String unidadReferente1; 
	private String nombreApellidosReferente1; 
	private String fuerzaReferente1; 
	private String cargoReferente1; 
	private String gradoReferente1;
	
	private String unidadReferente2; 
	private String nombreApellidosReferente2; 
	private String fuerzaReferente2; 
	private String cargoReferente2; 
	private String gradoReferente2;
	
	private String gradoReferente3;
	private String nombreApellidosReferente3; 
	private String fuerzaReferente3; 
	private String cargoReferente3;  
	private String unidadReferente3; 
	
	
	
	
}
