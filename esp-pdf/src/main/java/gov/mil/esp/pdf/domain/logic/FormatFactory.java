package gov.mil.esp.pdf.domain.logic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import gov.mil.esp.pdf.domain.logic.command.GenerateFormato;
import gov.mil.esp.pdf.domain.logic.command.ingreso.GenerateFormatoIngreso;

@Component
public class FormatFactory {
	
	@Autowired
	private ApplicationContext context;
	
	public GenerateFormato getFormat(String formatCommandName, String documentoIdentidad) {
		
		System.out.println("on the fabric -------------------------------------------------__>");
		
		//System.out.println(context.getBean("generateFormatoIngreso",GenerateFormatoIngreso.class));
		
		if (formatCommandName.equals("formatoAspirante")) {
			System.out.println("on getFormat --------->>");
			System.out.println(documentoIdentidad);
			GenerateFormatoIngreso generateFormatoIngreso = (GenerateFormatoIngreso) context.getBean("generateFormatoIngreso",documentoIdentidad);
			generateFormatoIngreso.setDocumentoIdentidad(documentoIdentidad);
			return  generateFormatoIngreso;
					
					//new GenerateFormatoIngreso(documentoIdentidad);
		}

		if (formatCommandName.equals("formatoRequerimiento")) {

		}

		if (formatCommandName.equals("formatoEsp")) {

		}
		return null;
	}

}
