package gov.mil.esp.pdf.model.formato.actualizacion;

import gov.mil.esp.pdf.model.formato.PaginaFormato;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Pagina5 extends PaginaFormato {
	
    private String fechaAntecedenteJudicial1;
    private String tipoAntecedenteJudicial1;
    private String causaAntecedenteJudicial1;
    private String autoridadAntecedenteJudicial1;
    private String estadoAntecedenteJudicial1;
    private String responsableAntecedenteJudicial1SI;
    private String responsableAntecedenteJudicial1NO;
    private String fechaAntecedenteJudicial2;
    private String tipoAntecedenteJudicial2;
    private String causaAntecedenteJudicial2;
    private String autoridadAntecedenteJudicial2;
    private String estadoAntecedenteJudicial2;
    private String responsableAntecedenteJudicial2SI;
    private String responsableAntecedenteJudicial2NO;
    private String datosAdicionales;
    private String firma;
    
	
}
