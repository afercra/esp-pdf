package gov.mil.esp.pdf.domain.logic.command.ingreso.pagina;

import org.springframework.stereotype.Component;

import gov.mil.esp.pdf.domain.logic.command.GeneratePage;
import gov.mil.esp.pdf.model.aspirante.DatosPersona;
import gov.mil.esp.pdf.model.formato.PaginaFormato;
import gov.mil.esp.pdf.model.formato.ingreso.Pagina1;

@Component
public class GeneratePaginaIngreso1 implements GeneratePage {

	@Override
	public PaginaFormato execute(DatosPersona datosPersona) {
		return Pagina1
				.builder()
				.dependenciaSolicitante(datosPersona.getDependenciaSolicitante())
				.empleCargoAspira(datosPersona.getCargoAspirante())
				.primerNombre(datosPersona.getPrimerNombre())
				.lugaryfecha("")
				.grado("")
				.cargoActual(datosPersona.getCargoAspirante())
				.profesion(datosPersona.getProfesion())
				.primerApellido(datosPersona.getPrimerApellido())
				.segundoApellido(datosPersona.getSegundoApellido())
				.segundoNombre(datosPersona.getSegundoNombre())
				.segundoApellido(datosPersona.getSegundoApellido())
				.numeroDocumento(datosPersona.getDocumentoIdentidad())
				.tipoDocumento(datosPersona.getTipoDocumentoIdentidad())
				.fechaYLugarExpedicion(getFechaYLugarDeExpedicionDocumentoIdentidad(datosPersona))
				.pasaporte(datosPersona.getNumeroPasaporte())
				.lugarExpedicionDocumento(getLugarDeExpedicionDocumentoIdentidad(datosPersona))
				.lugarNacimiento(getLugarNacimiento(datosPersona)).diaNacimiento(getDiaNacimiento(datosPersona))
				.mesNacimiento(getMesNacimiento(datosPersona))
				.anoNacimiento(getAnioNacimiento(datosPersona))
				.estadoCivil(datosPersona.getEstadoCivil())
				.gsrh(datosPersona.getGrupoSanguineo())
				.profesionUOficio(datosPersona.getProfesion()).tarjetaProfesional(datosPersona.getTarjetaProfesional())
				.nacionalidad(datosPersona.getNacionalidad())
				.estatura(datosPersona.getEstatura())
				.peso(datosPersona.getPeso())
				.build();

	}

	private String getFechaYLugarDeExpedicionDocumentoIdentidad(DatosPersona datosPersona) {
		return "10/01/10 " + getLugarDeExpedicionDocumentoIdentidad(datosPersona);
	}

	private String getLugarDeExpedicionDocumentoIdentidad(DatosPersona datosPersona) {
		return datosPersona.getCiudadExpedicionDocumentoIdentidad() + " "
				+ datosPersona.getDepartamentoExpedicionDocumentoIdentidad();
	}

	private String getLugarNacimiento(DatosPersona datosPersona) {
		return datosPersona.getPaisNacimiento() + ", " + datosPersona.getCiudadNacimiento() + ", "
				+ datosPersona.getDepartamentoNacimiento() + " ";
	}

	private String getAnioNacimiento(DatosPersona datosPersona) {
		if (datosPersona.getFechaNacimiento() != null) {
			if (datosPersona.getFechaNacimiento().split("/").length > 0) {
				return datosPersona.getFechaNacimiento().split("/")[0];
			}
		}
		return "";
	}

	private String getMesNacimiento(DatosPersona datosPersona) {
		if (datosPersona.getFechaNacimiento() != null) {
			if (datosPersona.getFechaNacimiento().split("/").length > 1) {
				return datosPersona.getFechaNacimiento().split("/")[1];
			}
		}
		return "";
	}

	private String getDiaNacimiento(DatosPersona datosPersona) {
		if (datosPersona.getFechaNacimiento() != null) {
			if (datosPersona.getFechaNacimiento().split("/").length > 1) {
				return datosPersona.getFechaNacimiento().split("/")[2];
			}
		}
		return "";
	}

}
