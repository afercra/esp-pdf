package gov.mil.esp.pdf.conf;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import feign.Contract;

@Configuration
public class PersonaClientConfiguration {

	@Bean
	public Contract useFeignAnnotations() {
		return new Contract.Default();
	}
	
}
