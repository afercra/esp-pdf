package gov.mil.esp.pdf.model.formato.actualizacion;

import gov.mil.esp.pdf.model.formato.PaginaFormato;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Pagina1 extends PaginaFormato{
	
	private String primerNombre;
	private String dependenciaSolicitante;
	private String lugaryfecha;
	private String grado;
	private String cargoActual;
	private String profesion;
	private String primerApellido;
	private String segundoApellido;
	private String segundoNombre;
	private String tipoDocumento;
	private String fechaYLugarExpedicion;
	private String pasaporte;
	private String lugarExpedicionDocumento;
	private String lugarNacimiento;
	private String diaNacimiento;
	private String mesNacimiento;
	private String anoNacimiento;
	private String estadoCivil;
	private String gsrh;
	private String profesionUOficio;
	private String tarjetaProfesional;
	private String nacionalidad;
	private String estatura;
	private String peso;
	private String nombresApellidosContacto1;
	private String nombresApellidosContacto2;
	private String direccionContacto1;
	private String direccionContacto2;
	private String numeroDocumento;
	

	
  
}
