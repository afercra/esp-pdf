package gov.mil.esp.pdf.model.aspirante;

import lombok.Data;

@Data
public class ObligacionCrediticia {
	
	String entidad;
	String valor;
	String concepto;
}
