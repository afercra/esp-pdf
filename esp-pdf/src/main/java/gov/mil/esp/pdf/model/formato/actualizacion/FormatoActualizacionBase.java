package gov.mil.esp.pdf.model.formato.actualizacion;

import gov.mil.esp.pdf.model.formato.FormatoGenerico;
import lombok.Data;

@Data
public class FormatoActualizacionBase extends FormatoGenerico {
	
	private Pagina1 pagina1;
	private Pagina2 pagina2;
	private Pagina3 pagina3;
	private Pagina4 pagina4;
	private Pagina5 pagina5;
	private Pagina6 pagina6;
	

}
