package gov.mil.esp.pdf.domain.logic.command.ingreso.pagina;

import org.springframework.stereotype.Component;

import gov.mil.esp.pdf.domain.logic.command.GeneratePage;
import gov.mil.esp.pdf.model.aspirante.DatosPersona;
import gov.mil.esp.pdf.model.formato.PaginaFormato;
import gov.mil.esp.pdf.model.formato.ingreso.Pagina7;

@Component
public class GeneratePaginaIngreso7 implements GeneratePage {

	@Override
	public PaginaFormato execute(DatosPersona datosPersona) {
		System.out.println("page7");
		return Pagina7.builder()
				
				.unidadRequiereEstudio("")
				.dependenciaRequiereEstudio("")
				.unidadSolicitaEstudio("")
				
				.build();
	}

}