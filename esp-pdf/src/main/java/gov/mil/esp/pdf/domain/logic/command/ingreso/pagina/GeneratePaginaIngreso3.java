package gov.mil.esp.pdf.domain.logic.command.ingreso.pagina;

import org.springframework.stereotype.Component;

import gov.mil.esp.pdf.domain.logic.command.GeneratePage;
import gov.mil.esp.pdf.model.aspirante.DatosPersona;
import gov.mil.esp.pdf.model.aspirante.Empresa;
import gov.mil.esp.pdf.model.aspirante.EstudioRealizado;
import gov.mil.esp.pdf.model.aspirante.Hermano;
import gov.mil.esp.pdf.model.aspirante.Referencia;
import gov.mil.esp.pdf.model.formato.PaginaFormato;
import gov.mil.esp.pdf.model.formato.ingreso.Pagina3;

@Component
public class GeneratePaginaIngreso3 implements GeneratePage {
	
	@Override
	public PaginaFormato execute(DatosPersona datosPersona) {
		System.out.println("page3");
		Hermano hermano1 = getHermano(datosPersona, 0);
		Hermano hermano2 = getHermano(datosPersona, 1);
		Hermano hermano3 = getHermano(datosPersona, 2);
		Hermano hermano4 = getHermano(datosPersona, 3); 
		
		Referencia contactoEmergencia1 = getContactoEmergencia(datosPersona, 0);
		Referencia contactoEmergencia2 = getContactoEmergencia(datosPersona, 1);
		 
		EstudioRealizado estudioRealizado1 = getEstudioRealizado(datosPersona, 0);
		EstudioRealizado estudioRealizado2 = getEstudioRealizado(datosPersona, 1);
		EstudioRealizado estudioRealizado3 = getEstudioRealizado(datosPersona, 2);
		EstudioRealizado estudioRealizado4 = getEstudioRealizado(datosPersona, 3);
		
		
		Empresa empresa1 = getEmpresa(datosPersona, 0);
		Empresa empresa2 = getEmpresa(datosPersona, 1);
		
		return Pagina3.builder()
				.primerApellidoHermano1((hermano1!=null)?hermano1.getPrimerApellido():"")
				.segundoApellidoHermano1((hermano1!=null)?hermano1.getSegundoApellido():"")
				.segundoNombreHermano1((hermano1!=null)?hermano1.getSegundoNombre():"")
				.primerNombreHermano1((hermano1!=null)?hermano1.getPrimerNombre():"")
				.ocupacionHermano1((hermano1!=null)?hermano1.getOcupacion():"")
				.numeroDocumentoHermano1((hermano1!=null)?hermano1.getNumeroDocumento():"")
				.direccionHermano1((hermano1!=null)?hermano1.getDireccion():"")
				.telefonoHermano1((hermano1!=null)?hermano1.getNumeroTelefono():"")
				.correoElectronicoHermano1((hermano1!=null)?hermano1.getCorreoElectronico():"")
				.redesSocialesHermano1((hermano1!=null)?hermano1.getRedesSociales():"")
				
				
				.primerApellidoHermano2((hermano2!=null)?hermano2.getPrimerApellido():"")
				.segundoApellidoHermano2((hermano2!=null)?hermano2.getSegundoApellido():"")
				.segundoNombreHermano2((hermano2!=null)?hermano2.getSegundoNombre():"")
				.primerNombreHermano2((hermano2!=null)?hermano2.getPrimerNombre():"")
				.ocupacionHermano2((hermano2!=null)?hermano2.getOcupacion():"")
				.numeroDocumentoHermano2((hermano2!=null)?hermano2.getNumeroDocumento():"")
				.direccionHermano2((hermano2!=null)?hermano2.getDireccion():"")
				.telefonoHermano2((hermano2!=null)?hermano2.getNumeroTelefono():"")
				.correoElectronicoHermano2((hermano2!=null)?hermano2.getCorreoElectronico():"")
				.redesSocialesHermano2((hermano2!=null)?hermano2.getRedesSociales():"")

				.primerApellidoHermano3((hermano3!=null)?hermano3.getPrimerApellido():"")
				.segundoApellidoHermano3((hermano3!=null)?hermano3.getSegundoApellido():"")
				.segundoNombreHermano3((hermano3!=null)?hermano3.getSegundoNombre():"")
				.primerNombreHermano3((hermano3!=null)?hermano3.getPrimerNombre():"")
				.ocupacionHermano3((hermano3!=null)?hermano3.getOcupacion():"")
				.numeroDocumentoHermano3((hermano3!=null)?hermano3.getNumeroDocumento():"")
				.direccionHermano3((hermano3!=null)?hermano3.getDireccion():"")
				.telefonoHermano3((hermano3!=null)?hermano3.getNumeroTelefono():"")
				.correoElectronicoHermano3((hermano3!=null)?hermano3.getCorreoElectronico():"")
				.redesSocialesHermano3((hermano3!=null)?hermano3.getRedesSociales():"")
				
				
				.primerApellidoHermano4((hermano4!=null)?hermano4.getPrimerApellido():"")
				.segundoApellidoHermano4((hermano4!=null)?hermano4.getSegundoApellido():"")
				.segundoNombreHermano4((hermano4!=null)?hermano4.getSegundoNombre():"")
				.primerNombreHermano4((hermano4!=null)?hermano4.getPrimerNombre():"")
				.ocupacionHermano4((hermano4!=null)?hermano4.getOcupacion():"")
				.numeroDocumentoHermano4((hermano4!=null)?hermano4.getNumeroDocumento():"")
				.direccionHermano4((hermano4!=null)?hermano4.getDireccion():"")
				.telefonoHermano4((hermano4!=null)?hermano4.getNumeroTelefono():"")
				.correoElectronicoHermano4((hermano4!=null)?hermano4.getCorreoElectronico():"")
				.redesSocialesHermano4((hermano4!=null)?hermano4.getRedesSociales():"")
				
				
				.nombresApellidosContacto1((contactoEmergencia1!=null)?getNombreCompletoReferencia(contactoEmergencia1):"")
				.nombresApellidosContacto2((contactoEmergencia2!=null)?getNombreCompletoReferencia(contactoEmergencia2):"")
				.telefonoContacto1((contactoEmergencia1!=null)?contactoEmergencia1.getNumeroTelefono():"")
				.telefonoContacto2((contactoEmergencia2!=null)?contactoEmergencia2.getNumeroTelefono():"")
				

				
				.estudiosRealizados1((estudioRealizado1!= null)?estudioRealizado1.getEstudioRealizado():"")
				.anoEstudiosRealizados1((estudioRealizado1!= null)?estudioRealizado1.getAnioFinalizacion():"")
				.tituloObtenido1((estudioRealizado1!= null)?estudioRealizado1.getTituloObtenido():"")
				.nombreInstitucion1((estudioRealizado1!= null)?estudioRealizado1.getNombreInstitucion():"")
				.ciudadPaisEstudio1((estudioRealizado1!= null)?estudioRealizado1.getCiudad()+" "+estudioRealizado1.getPaisInstitucion():"")
				
				.estudiosRealizados2((estudioRealizado2!= null)?estudioRealizado2.getEstudioRealizado():"")
				.anoEstudiosRealizados2((estudioRealizado2!= null)?estudioRealizado2.getAnioFinalizacion():"")
				.tituloObtenido2((estudioRealizado2!= null)?estudioRealizado2.getTituloObtenido():"")
				.nombreInstitucion2((estudioRealizado2!= null)?estudioRealizado2.getNombreInstitucion():"")
				.ciudadPaisEstudio2((estudioRealizado2!= null)?estudioRealizado2.getCiudad()+" "+estudioRealizado2.getPaisInstitucion():"")
				
				.estudiosRealizados3((estudioRealizado3!= null)?estudioRealizado3.getEstudioRealizado():"")
				.anoEstudiosRealizados3((estudioRealizado3!= null)?estudioRealizado3.getAnioFinalizacion():"")
				.tituloObtenido3((estudioRealizado3!= null)?estudioRealizado3.getTituloObtenido():"")
				.nombreInstitucion3((estudioRealizado3!= null)?estudioRealizado3.getNombreInstitucion():"")
				.ciudadPaisEstudio3((estudioRealizado3!= null)?estudioRealizado3.getCiudad()+" "+estudioRealizado3.getPaisInstitucion():"")
				
				.estudiosRealizados4((estudioRealizado4!= null)?estudioRealizado4.getEstudioRealizado():"")
				.anoEstudiosRealizados4((estudioRealizado4!= null)?estudioRealizado4.getAnioFinalizacion():"")
				.tituloObtenido4((estudioRealizado4!= null)?estudioRealizado4.getTituloObtenido():"")
				.nombreInstitucion4((estudioRealizado4!= null)?estudioRealizado4.getNombreInstitucion():"")
				.ciudadPaisEstudio4((estudioRealizado4!= null)?estudioRealizado4.getCiudad()+" "+estudioRealizado4.getPaisInstitucion():"")
				
				.idiomaExtranjero((datosPersona.getIdiomas()!=null)?buildIdioma(datosPersona):"")

				
				.nombreEmpresa1((empresa1!=null)?empresa1.getNombre():"")
				.cargoEmpresa1((empresa1!=null)?empresa1.getCargo():"")
				.tiempoEmpresa1((empresa1!=null)?empresa1.getTiempoEnEmpresa():"")
				.telefonosEmpresa1((empresa1!=null)?empresa1.getNumeroTelefono():"")
				.direccionEmpresa1((empresa1!=null)?empresa1.getDireccion():"")
				.sueldoEmpresa1((empresa1!=null)?empresa1.getSueldo():"")
				.motivoRetiroEmpresa1((empresa1!=null)?empresa1.getMotivoRetiro():"")
				.nombreJefeEmpresa1((empresa1!=null)?empresa1.getNombreJefe():"")
				.paginaWebEmpresa1((empresa1!=null)?empresa1.getPaginaWeb():"")
				
				.nombreEmpresa2((empresa2!=null)?empresa2.getNombre():"")
				.cargoEmpresa2((empresa2!=null)?empresa2.getCargo():"")
				.tiempoEmpresa2((empresa2!=null)?empresa2.getTiempoEnEmpresa():"")
				.telefonosEmpresa2((empresa2!=null)?empresa2.getNumeroTelefono():"")
				.direccionEmpresa2((empresa2!=null)?empresa2.getDireccion():"")
				.sueldoEmpresa2((empresa2!=null)?empresa2.getSueldo():"")
				.motivoRetiroEmpresa2((empresa2!=null)?empresa2.getMotivoRetiro():"")
				.nombreJefeEmpresa2((empresa2!=null)?empresa2.getNombreJefe():"")
				.paginaWebEmpresa2((empresa2!=null)?empresa2.getPaginaWeb():"")
				

				.build();

	}
	
	private Hermano getHermano(DatosPersona datosPersona, int posicion) {
		System.out.println("get hermano "+ posicion);
		
		if(datosPersona.getHermanos()!=null)
		 {
			if(datosPersona.getHermanos().size() > posicion) {
				System.out.println("inside hermanos  ------>");
			System.out.println(datosPersona.getHermanos().size());
			System.out.println(posicion);
				System.out.println(datosPersona.getHermanos().get(posicion));
				return datosPersona.getHermanos().get(posicion);
				}
		 }
		
		return null;
	}
	
	private Referencia getContactoEmergencia(DatosPersona datosPersona, int posicion) {
		if(datosPersona.getContactoEmergencia()!=null)
		 {
			if(datosPersona.getContactoEmergencia().size() > posicion)
				return datosPersona.getContactoEmergencia().get(posicion);
		 }
		
		return null;
	}	
	
	private EstudioRealizado getEstudioRealizado(DatosPersona datosPersona, int posicion) {
		if(datosPersona.getEstudiosRealizados()!=null)
		 {
			if(datosPersona.getEstudiosRealizados().size() > posicion)
				return datosPersona.getEstudiosRealizados().get(posicion);
		 }
		
		return null;
	}	
	
	private Empresa getEmpresa(DatosPersona datosPersona, int posicion) {
		if(datosPersona.getEmpresas()!=null)
		 {
			if(datosPersona.getEmpresas().size() > posicion)
				return datosPersona.getEmpresas().get(posicion);
		 }
		
		return null;
	}	
	
	
	private String getNombreCompletoReferencia(Referencia referencia) {
		return referencia.getPrimerNombre()+" "+referencia.getSegundoNombre()
		+" "+referencia.getPrimerApellido()+" "+referencia.getSegundoApellido();
	}
	
	private String buildIdioma(DatosPersona datosPersona) {
		StringBuilder sb = new StringBuilder();
		datosPersona.getIdiomas().forEach(x->{
			sb.append(x.getNombre()+",");
		});
		
		return sb.toString();
	}

}