package gov.mil.esp.pdf.model.aspirante;

import lombok.Data;

@Data
public class OtroIngreso {
	
	String concepto;
	String valor;

}
