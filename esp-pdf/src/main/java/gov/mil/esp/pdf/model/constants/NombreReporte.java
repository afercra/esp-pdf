package gov.mil.esp.pdf.model.constants;

public enum NombreReporte {
	
	FORMATO_ACTUALIZACION("formatoactualizacion.jasper"),
	FORMATO_INGRESO("formatoIngreso.jasper"),
	FORMATO_REQUERIMIENTO("formatoRequerimiento.jasper"),
	FORMATO_ESP("formatoEsp.jasper"),
	FORMATO_AVAL("formatoAval.jasper");
	
	private String nombreFormato;

	public String getNombreFormato() {
		return nombreFormato;
	}

	private NombreReporte(String nombreFormato) {
		this.nombreFormato = nombreFormato;
	}
	
	

}
