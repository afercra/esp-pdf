package gov.mil.esp.pdf.provider.file;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import gov.mil.esp.pdf.provider.jasper.JasperBuilder;
import gov.mil.esp.pdf.provider.jasper.JasperBuilderImpl;
import net.sf.jasperreports.engine.JRException;

@Component
public class FileBuilderImpl implements FileBuilder{
	
	
	JasperBuilder jasperBuilder;
	
	@Autowired
	public FileBuilderImpl(JasperBuilderImpl jasperBuilderImpl) {
		this.jasperBuilder = jasperBuilderImpl;
	}
	
	@Override	
	public byte [] buildFormato(String nombreFormato, String parametros) throws IOException, JRException {
		return jasperBuilder.buildPdfFile(nombreFormato, parametros);
	}
	

}
