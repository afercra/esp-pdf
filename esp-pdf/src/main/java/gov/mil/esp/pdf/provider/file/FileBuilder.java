package gov.mil.esp.pdf.provider.file;

import java.io.IOException;

import org.springframework.stereotype.Component;

import net.sf.jasperreports.engine.JRException;

@Component
public interface FileBuilder {

	byte [] buildFormato(String nombreFormato, String parametros) throws IOException, JRException;
}
