package gov.mil.esp.pdf.domain.logic.command.actualizacion.pagina;

import org.springframework.stereotype.Component;

import gov.mil.esp.pdf.domain.logic.command.GeneratePage;
import gov.mil.esp.pdf.model.aspirante.DatosPersona;
import gov.mil.esp.pdf.model.aspirante.Hermano;
import gov.mil.esp.pdf.model.aspirante.Referencia;
import gov.mil.esp.pdf.model.formato.PaginaFormato;
import gov.mil.esp.pdf.model.formato.ingreso.Pagina3;

@Component
public class GeneratePaginaActualizacion3 implements GeneratePage {
	
	@Override
	public PaginaFormato execute(DatosPersona datosPersona) {
	
		Hermano hermano1 = getHermano(datosPersona, 0);
		Hermano hermano2 = getHermano(datosPersona, 1);
		Hermano hermano3 = getHermano(datosPersona, 2);
		Hermano hermano4 = getHermano(datosPersona, 3);
		
		Referencia contactoEmergencia1 = getContactoEmergencia(datosPersona, 0);
		Referencia contactoEmergencia2 = getContactoEmergencia(datosPersona, 1);
		
		return Pagina3.builder()
				.primerApellidoHermano1((hermano1!=null)?hermano1.getPrimerApellido():"")
				.segundoApellidoHermano1((hermano1!=null)?hermano1.getSegundoApellido():"")
				.segundoNombreHermano1((hermano1!=null)?hermano1.getSegundoNombre():"")
				.primerNombreHermano1((hermano1!=null)?hermano1.getPrimerNombre():"")
				.ocupacionHermano1((hermano1!=null)?hermano1.getOcupacion():"")
				.numeroDocumentoHermano1((hermano1!=null)?hermano1.getNumeroDocumento():"")
				.direccionHermano1((hermano1!=null)?hermano1.getDireccion():"")
				.telefonoHermano1((hermano1!=null)?hermano1.getNumeroTelefono():"")
				.correoElectronicoHermano1((hermano1!=null)?hermano1.getCorreoElectronico():"")
				.redesSocialesHermano1((hermano1!=null)?hermano1.getRedesSociales():"")
				
				
				.primerApellidoHermano2((hermano2!=null)?hermano2.getPrimerApellido():"")
				.segundoApellidoHermano2((hermano2!=null)?hermano2.getSegundoApellido():"")
				.segundoNombreHermano2((hermano2!=null)?hermano2.getSegundoNombre():"")
				.primerNombreHermano2((hermano2!=null)?hermano2.getPrimerNombre():"")
				.ocupacionHermano2((hermano2!=null)?hermano2.getOcupacion():"")
				.numeroDocumentoHermano2((hermano2!=null)?hermano2.getNumeroDocumento():"")
				.direccionHermano2((hermano2!=null)?hermano2.getDireccion():"")
				.telefonoHermano2((hermano2!=null)?hermano2.getNumeroTelefono():"")
				.correoElectronicoHermano2((hermano2!=null)?hermano2.getCorreoElectronico():"")
				.redesSocialesHermano2((hermano2!=null)?hermano2.getRedesSociales():"")

				.primerApellidoHermano3((hermano3!=null)?hermano3.getPrimerApellido():"")
				.segundoApellidoHermano3((hermano3!=null)?hermano3.getSegundoApellido():"")
				.segundoNombreHermano3((hermano3!=null)?hermano3.getSegundoNombre():"")
				.primerNombreHermano3((hermano3!=null)?hermano3.getPrimerNombre():"")
				.ocupacionHermano3((hermano3!=null)?hermano3.getOcupacion():"")
				.numeroDocumentoHermano3((hermano3!=null)?hermano3.getNumeroDocumento():"")
				.direccionHermano3((hermano3!=null)?hermano3.getDireccion():"")
				.telefonoHermano3((hermano3!=null)?hermano3.getNumeroTelefono():"")
				.correoElectronicoHermano3((hermano3!=null)?hermano3.getCorreoElectronico():"")
				.redesSocialesHermano3((hermano3!=null)?hermano3.getRedesSociales():"")
				
				
				.primerApellidoHermano4((hermano4!=null)?hermano4.getPrimerApellido():"")
				.segundoApellidoHermano4((hermano4!=null)?hermano4.getSegundoApellido():"")
				.segundoNombreHermano4((hermano4!=null)?hermano4.getSegundoNombre():"")
				.primerNombreHermano4((hermano4!=null)?hermano4.getPrimerNombre():"")
				.ocupacionHermano4((hermano4!=null)?hermano4.getOcupacion():"")
				.numeroDocumentoHermano4((hermano4!=null)?hermano4.getNumeroDocumento():"")
				.direccionHermano4((hermano4!=null)?hermano4.getDireccion():"")
				.telefonoHermano4((hermano4!=null)?hermano4.getNumeroTelefono():"")
				.correoElectronicoHermano4((hermano4!=null)?hermano4.getCorreoElectronico():"")
				.redesSocialesHermano4((hermano4!=null)?hermano4.getRedesSociales():"")
				
				
				.nombresApellidosContacto1((contactoEmergencia1!=null)?getNombreCompletoReferencia(contactoEmergencia1):"")
				.nombresApellidosContacto2((contactoEmergencia2!=null)?getNombreCompletoReferencia(contactoEmergencia2):"")
				.telefonoContacto1((contactoEmergencia1!=null)?contactoEmergencia1.getNumeroTelefono():"")
				.telefonoContacto2((contactoEmergencia2!=null)?contactoEmergencia2.getNumeroTelefono():"")
				
				
				.nombresApellidosReferencia1("")
				.ocupacionReferencia1("")
				.direccionReferencia1("")
				.telefonosReferencia1("")
				
				.nombresApellidosReferencia2("")
				.ocupacionReferencia2("")
				.direccionReferencia2("")
				.telefonosReferencia2("")
				
				.estudiosRealizados1("")
				.anoEstudiosRealizados1("")
				.tituloObtenido1("")
				.nombreInstitucion1("")
				.ciudadPaisEstudio1("")
				
				.estudiosRealizados2("")
				.anoEstudiosRealizados2("")
				.tituloObtenido2("")
				.nombreInstitucion2("")
				.ciudadPaisEstudio2("")
				
				.estudiosRealizados3("")
				.anoEstudiosRealizados3("")
				.tituloObtenido3("")
				.nombreInstitucion3("")
				.ciudadPaisEstudio3("")
				
				.estudiosRealizados4("")
				.anoEstudiosRealizados4("")
				.tituloObtenido4("")
				.nombreInstitucion4("")
				.ciudadPaisEstudio4("")
				
				
				.curso1("")
				.especialidadMilitar1("")
				.condecoracionYMedalla1("")
				.curso2("")
				.especialidadMilitar2("")
				.condecoracionYMedalla2("")
				.curso3("")
				.especialidadMilitar3("")
				.condecoracionYMedalla3("")
				.unidadOrigenActualizacionNombre("")
				.unidadOrigenDesde("")
				.unidadOrigenHasta("")
				.unidadOrigenCargoOcupado("")
				
				.nombreEmpresa1("")
				.cargoEmpresa1("")
				.tiempoEmpresa1("")
				.telefonosEmpresa1("")
				.sueldoEmpresa1("")
				.motivoRetiroEmpresa1("")
				.nombreJefeEmpresa1("")
				.paginaWebEmpresa1("")
				.nombreEmpresa2("")
				.cargoEmpresa2("")
				.tiempoEmpresa2("")
				.telefonosEmpresa2("")
				.sueldoEmpresa2("")
				.motivoRetiroEmpresa2("")
				.nombreJefeEmpresa2("")
				.paginaWebEmpresa2("")
				.direccionEmpresa1("")
				.direccionEmpresa2("")
				
				.idiomaExtranjero("")

				.build();

	}
	
	private Hermano getHermano(DatosPersona datosPersona, int posicion) {
		if(datosPersona.getHermanos()!=null)
		 {
			if(datosPersona.getHermanos().size() > posicion)
				return datosPersona.getHermanos().get(posicion);
		 }
		
		return null;
	}
	
	private Referencia getContactoEmergencia(DatosPersona datosPersona, int posicion) {
		if(datosPersona.getContactoEmergencia()!=null)
		 {
			if(datosPersona.getContactoEmergencia().size() > posicion)
				return datosPersona.getContactoEmergencia().get(posicion);
		 }
		
		return null;
	}	
	
	
	private String getNombreCompletoReferencia(Referencia referencia) {
		return referencia.getPrimerNombre()+" "+referencia.getSegundoNombre()
		+" "+referencia.getPrimerApellido()+" "+referencia.getSegundoApellido();
	}

}