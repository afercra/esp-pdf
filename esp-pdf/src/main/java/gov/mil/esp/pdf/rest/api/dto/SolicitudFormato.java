package gov.mil.esp.pdf.rest.api.dto;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class SolicitudFormato implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@NotNull(message = "Debe ingresar un tipo de formato a generar")
	@NotBlank
	String tipoFormato;
	@NotNull(message = "Debe ingresar un tipo de formato a generar")
	@NotBlank
	String numeroDocumento;

}
