package gov.mil.esp.pdf.model.aspirante;

import lombok.Data;

@Data
public class Empresa {

	String nombre;
	String direccion;
	String cargo;
	String tiempoEnEmpresa;
	String numeroCelular;
	String numeroTelefono;
	String sueldo;
	String motivoRetiro;
	String nombreJefe;
	String paginaWeb;
	String actividad;

}
