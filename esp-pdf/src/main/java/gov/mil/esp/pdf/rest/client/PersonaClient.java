package gov.mil.esp.pdf.rest.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;

import feign.Headers;
import feign.Param;
import feign.RequestLine;
import gov.mil.esp.pdf.conf.PersonaClientConfiguration;
import gov.mil.esp.pdf.model.aspirante.DatosPersona;
import gov.mil.esp.pdf.rest.api.Response;
import gov.mil.esp.pdf.rest.client.fallback.PersonaClientFallbackFactory;


@Component
@FeignClient(url="${conf.esp.client.aspirante.url}", name="personaClient",configuration = PersonaClientConfiguration.class, fallbackFactory = PersonaClientFallbackFactory.class )
public interface PersonaClient {
	
	//@RequestLine("GET /")
    //Response<DatosPersona> getDatosPersona( Request<String> usuario);
	
	@RequestLine(value="GET /persona/datos/{documentoIdentidad}")
	@Headers("Content-Type: application/json")
	Response<DatosPersona> getDatosPersona(@Param("documentoIdentidad") String documentoIdentidad);
	
}
