package gov.mil.esp.pdf.domain.logic.command.ingreso.pagina;

import org.springframework.stereotype.Component;

import gov.mil.esp.pdf.domain.logic.command.GeneratePage;
import gov.mil.esp.pdf.model.aspirante.ActividadEconomicaPrivada;
import gov.mil.esp.pdf.model.aspirante.DatosPersona;
import gov.mil.esp.pdf.model.aspirante.Empresa;
import gov.mil.esp.pdf.model.aspirante.OrganismoEstatal;
import gov.mil.esp.pdf.model.aspirante.Referencia;
import gov.mil.esp.pdf.model.aspirante.ViajeExterior;
import gov.mil.esp.pdf.model.formato.PaginaFormato;
import gov.mil.esp.pdf.model.formato.ingreso.Pagina5;

@Component
public class GeneratePaginaIngreso5 implements GeneratePage {

	@Override
	public PaginaFormato execute(DatosPersona datosPersona) {
		System.out.println("page5");
		ActividadEconomicaPrivada actividadEconomicaPrivada1 = getActividadEconomicaPrivada(datosPersona, 0);
		ActividadEconomicaPrivada actividadEconomicaPrivada2 = getActividadEconomicaPrivada(datosPersona, 1);
		ActividadEconomicaPrivada actividadEconomicaPrivada3 = getActividadEconomicaPrivada(datosPersona, 2);

		ViajeExterior viaje1 = getViajeAlExterior(datosPersona, 0);
		ViajeExterior viaje2 = getViajeAlExterior(datosPersona, 1);
		ViajeExterior viaje3 = getViajeAlExterior(datosPersona, 2);
		ViajeExterior viaje4 = getViajeAlExterior(datosPersona, 3);

		Referencia parienteOAmigoEnLaInstitucion1 = getParienteOAmigoEnLaInstitucion(datosPersona, 0);
		Referencia parienteOAmigoEnLaInstitucion2 = getParienteOAmigoEnLaInstitucion(datosPersona, 1);
		Referencia parienteOAmigoEnLaInstitucion3 = getParienteOAmigoEnLaInstitucion(datosPersona, 2);

		OrganismoEstatal organismoEstatal1 = getOrganismoEstatal(datosPersona, 0);
		OrganismoEstatal organismoEstatal2 = getOrganismoEstatal(datosPersona, 1);

		return Pagina5.builder()

				.empresaRazonSocial1(
						(actividadEconomicaPrivada1 != null) ? actividadEconomicaPrivada1.getRazonSocial() : "")
				.empresaRazonSocial2(
						(actividadEconomicaPrivada2 != null) ? actividadEconomicaPrivada2.getRazonSocial() : "")
				.empresaRazonSocial3(
						(actividadEconomicaPrivada3 != null) ? actividadEconomicaPrivada3.getRazonSocial() : "")
				.actividadEmpresa1(
						(actividadEconomicaPrivada1 != null) ? actividadEconomicaPrivada1.getActividad() : "")
				.actividadEmpresa2(
						(actividadEconomicaPrivada2 != null) ? actividadEconomicaPrivada2.getActividad() : "")
				.actividadEmpresa3(
						(actividadEconomicaPrivada3 != null) ? actividadEconomicaPrivada3.getActividad() : "")

				.descripcionActividadesViajeExterior(datosPersona.getActividadesEnElexterior())

				.motivoViajeExterior1((viaje1 != null) ? viaje1.getMotivo() : "")
				.paisViajeExterior1((viaje1 != null) ? viaje1.getPaisVisitado() : "")
				.fechaViajeExterior1((viaje1 != null) ? viaje1.getFecha() : "")
				.tiempoPermanenciaViajeExterior1((viaje1 != null) ? viaje1.getTiempoPermanencia() : "")

				.motivoViajeExterior2((viaje2 != null) ? viaje2.getMotivo() : "")
				.paisViajeExterior2((viaje2 != null) ? viaje2.getPaisVisitado() : "")
				.fechaViajeExterior2((viaje2 != null) ? viaje2.getFecha() : "")
				.tiempoPermanenciaViajeExterior2((viaje2 != null) ? viaje2.getTiempoPermanencia() : "")

				.motivoViajeExterior3((viaje3 != null) ? viaje3.getMotivo() : "")
				.paisViajeExterior3((viaje3 != null) ? viaje3.getPaisVisitado() : "")
				.fechaViajeExterior3((viaje3 != null) ? viaje3.getFecha() : "")
				.tiempoPermanenciaViajeExterior3((viaje3 != null) ? viaje3.getTiempoPermanencia() : "")

				.motivoViajeExterior4((viaje4 != null) ? viaje4.getMotivo() : "")
				.paisViajeExterior4((viaje4 != null) ? viaje4.getPaisVisitado() : "")
				.fechaViajeExterior4((viaje4 != null) ? viaje4.getFecha() : "")
				.tiempoPermanenciaViajeExterior4((viaje4 != null) ? viaje4.getTiempoPermanencia() : "")

				.armasManipuladasEnCompaniaDe(datosPersona.getArmasManipuladasEnCompaniaDe())

				.entidadRelacionada1((organismoEstatal1 != null) ? organismoEstatal1.getEntidad() : "")
				.motivoRetiroEntidadRelacionada1((organismoEstatal1 != null) ? organismoEstatal1.getMotivoRetiro() : "")
				.cargoEntidadRelacionada1((organismoEstatal1 != null) ? organismoEstatal1.getCargo() : "")

				.entidadRelacionada2((organismoEstatal2 != null) ? organismoEstatal2.getEntidad() : "")
				.motivoRetiroEntidadRelacionada2((organismoEstatal2 != null) ? organismoEstatal2.getMotivoRetiro() : "")
				.cargoEntidadRelacionada2((organismoEstatal2 != null) ? organismoEstatal2.getCargo() : "")

				.conoceArmasDeFuegoExplosivosSI("").conoceArmasDeFuegoExplosivosNO("")

				.conoceArmasDeFuegoExplosivosCuando(datosPersona.getConoceArmasDeFuegoExplosivosCuando())
				.conoceArmasDeFuegoExplosivosCuandoYDonde(datosPersona.getConoceArmasDeFuegoExplosivosCuandoYDonde())
				.conoceArmasDeFuegoExplosivosCualesHaManipulado(
						datosPersona.getConoceArmasDeFuegoExplosivosCualesHaManipulado())
				.conoceArmasDeFuegoExplosivosCuales(datosPersona.getConoceArmasDeFuegoExplosivosCuales())
				.armasManipuladasEnCompaniaDe(datosPersona.getArmasManipuladasEnCompaniaDe())
				.opinionGruposMarginales(datosPersona.getOpinionSobreOrganizacionesIlegales())

				.tieneInformacionRiesgosaParaNacionSI(getTieneInformacionRiesgosaParaNacionSi(datosPersona.getTieneInformacionRiesgosaParaNacion()))
				.tieneInformacionRiesgosaParaNacionNO(getTieneInformacionRiesgosaParaNacionNo(datosPersona.getTieneInformacionRiesgosaParaNacion()))
				
				.informacionRiesgosaParaNacion(datosPersona.getInformacionAmenazanteParaNacion()+"")

				.tieneInformacionActosCorruptosSI(getTieneInformacionActosCorrupcionEnInstitucionSi(datosPersona.getTieneInformacionActosCorrupcionEnInstitucion()))
				.tieneInformacionActosCorruptosNO(getTieneInformacionActosCorrupcionEnInstitucionNo(datosPersona.getTieneInformacionActosCorrupcionEnInstitucion()))

				.informacionActosCorrupcionEnInstitucion(datosPersona.getInformacionActosCorrupcionEnInstitucion()+ " ")

				.recomendadoPor(datosPersona.getEmpleoSugeridoPorNombreCompleto())
				.telefonoReferente(datosPersona.getEmpleosugeridoPorTelefono())
				.direccionReferente(datosPersona.getEmpleosugeridoPorDireccion())
				
				.recomendadoPorAlguienDeLaInstitucionSI(getEsRecomendadoPorAlguienDeLaInstitucionSi(datosPersona.getEsRecomendadoporAlguienDeLaInstitucion()))
				.recomendadoPorAlguienDeLaInstitucionNO(getEsRecomendadoPorAlguienDeLaInstitucionNo(datosPersona.getEsRecomendadoporAlguienDeLaInstitucion()))
				.nombreApellidosReferente1(
						(parienteOAmigoEnLaInstitucion1 != null) ? parienteOAmigoEnLaInstitucion1.getNombreCompleto()
								: "")
				.fuerzaReferente1(
						(parienteOAmigoEnLaInstitucion1 != null) ? parienteOAmigoEnLaInstitucion1.getFuerza() : "")
				.cargoReferente1(
						(parienteOAmigoEnLaInstitucion1 != null) ? parienteOAmigoEnLaInstitucion1.getCargo() : "")
				.gradoReferente1(
						(parienteOAmigoEnLaInstitucion1 != null) ? parienteOAmigoEnLaInstitucion1.getGrado() : "")
				.unidadReferente1(
						(parienteOAmigoEnLaInstitucion1 != null) ? parienteOAmigoEnLaInstitucion1.getUnidad() : "")

				.nombreApellidosReferente2(
						(parienteOAmigoEnLaInstitucion2 != null) ? parienteOAmigoEnLaInstitucion2.getNombreCompleto()
								: "")
				.fuerzaReferente2(
						(parienteOAmigoEnLaInstitucion2 != null) ? parienteOAmigoEnLaInstitucion2.getFuerza() : "")
				.cargoReferente2(
						(parienteOAmigoEnLaInstitucion2 != null) ? parienteOAmigoEnLaInstitucion2.getCargo() : "")
				.gradoReferente2(
						(parienteOAmigoEnLaInstitucion2 != null) ? parienteOAmigoEnLaInstitucion2.getGrado() : "")
				.unidadReferente2(
						(parienteOAmigoEnLaInstitucion2 != null) ? parienteOAmigoEnLaInstitucion2.getUnidad() : "")

				.nombreApellidosReferente3(
						(parienteOAmigoEnLaInstitucion3 != null) ? parienteOAmigoEnLaInstitucion3.getNombreCompleto()
								: "")
				.fuerzaReferente3(
						(parienteOAmigoEnLaInstitucion3 != null) ? parienteOAmigoEnLaInstitucion3.getFuerza() : "")
				.cargoReferente3(
						(parienteOAmigoEnLaInstitucion3 != null) ? parienteOAmigoEnLaInstitucion3.getCargo() : "")
				.gradoReferente3(
						(parienteOAmigoEnLaInstitucion3 != null) ? parienteOAmigoEnLaInstitucion3.getGrado() : "")
				.unidadReferente3(
						(parienteOAmigoEnLaInstitucion3 != null) ? parienteOAmigoEnLaInstitucion3.getUnidad() : "")

				.build();
	}

	private ActividadEconomicaPrivada getActividadEconomicaPrivada(DatosPersona datosPersona, int indice) {
		if (datosPersona.getActividadEconomicaPrivada() != null) {
			if (datosPersona.getActividadEconomicaPrivada().size() > indice) {
				return datosPersona.getActividadEconomicaPrivada().get(indice);
			}
		}
		return null;
	}

	private OrganismoEstatal getOrganismoEstatal(DatosPersona datosPersona, int indice) {
		if (datosPersona.getOrganizacionesGubarnamentales() != null) {
			if (datosPersona.getOrganizacionesGubarnamentales().size() > indice) {
				return datosPersona.getOrganizacionesGubarnamentales().get(indice);
			}
		}
		return null;
	}

	private ViajeExterior getViajeAlExterior(DatosPersona datosPersona, int indice) {
		if (datosPersona.getViajesAlExterior() != null) {
			if (datosPersona.getViajesAlExterior().size() > indice) {
				return datosPersona.getViajesAlExterior().get(indice);
			}
		}
		return null;
	}

	private Referencia getParienteOAmigoEnLaInstitucion(DatosPersona datosPersona, int indice) {
		if (datosPersona.getParientesOAmigosEnLaInstitucion() != null) {
			if (datosPersona.getParientesOAmigosEnLaInstitucion().size() > indice) {
				return datosPersona.getParientesOAmigosEnLaInstitucion().get(indice);
			}
		}
		return null;
	}

	private String getTieneInformacionRiesgosaParaNacionSi(String tieneInformacionAmenazanteANacion) {
		if (tieneInformacionAmenazanteANacion != null) {
			return (tieneInformacionAmenazanteANacion.equalsIgnoreCase("si")) ? "X" : "";

		}
		return "";
	}

	private String getTieneInformacionRiesgosaParaNacionNo(String tieneInformacionAmenazanteANacion) {
		if (tieneInformacionAmenazanteANacion != null) {
			return (tieneInformacionAmenazanteANacion.equalsIgnoreCase("no")) ? "X" : "";
		}

		return "";
	}
	
	private String getTieneInformacionActosCorrupcionEnInstitucionSi(String tieneInformacionActosCorrupcionEnInstitucion) {
		if (tieneInformacionActosCorrupcionEnInstitucion != null) {
			return (tieneInformacionActosCorrupcionEnInstitucion.equalsIgnoreCase("si")) ? "X" : "";

		}
		return "";
	}

	private String getTieneInformacionActosCorrupcionEnInstitucionNo(String tieneInformacionActosCorrupcionEnInstitucion) {
		if (tieneInformacionActosCorrupcionEnInstitucion != null) {
			return (tieneInformacionActosCorrupcionEnInstitucion.equalsIgnoreCase("no")) ? "X" : "";
		}

		return "";
	}
	
	private String getEsRecomendadoPorAlguienDeLaInstitucionSi(String recomendadoPorAlguienDeLaInstitucion) {
		if (recomendadoPorAlguienDeLaInstitucion != null) {
			return (recomendadoPorAlguienDeLaInstitucion.equalsIgnoreCase("si")) ? "X" : "";

		}
		return "";
	}

	private String getEsRecomendadoPorAlguienDeLaInstitucionNo(String recomendadoPorAlguienDeLaInstitucion) {
		if (recomendadoPorAlguienDeLaInstitucion != null) {
			return (recomendadoPorAlguienDeLaInstitucion.equalsIgnoreCase("no")) ? "X" : "";
		}

		return "";
	}

}