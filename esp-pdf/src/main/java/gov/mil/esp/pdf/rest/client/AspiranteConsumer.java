package gov.mil.esp.pdf.rest.client;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import gov.mil.esp.pdf.rest.api.Request;

@Component
public class AspiranteConsumer {


	@Value("${conf.esp.client.aspirante.url}")
	private String datosAspiranteUrl;

	@PostConstruct
	public void init() {
//		restTemplate = new RestTemplate();
	}

	public void getAspiranteDatos(String numeroDocumento) {
		Request<String> request = new Request<String>(numeroDocumento);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity httpRequest = new HttpEntity<>(request, headers);
		
		//Response<DatosAspirante> response = restTemplate.getForObject(datosAspiranteUrl +
			//	  "/datosAspirante", httpRequest, Response.class);
	}

	/*
	 * public void enviarDocumentoAColaDeInicioProcesos(String numeroDocumento) {
	 * System.out.println(numeroDocumento); System.out.println(procesoUrl);
	 * 
	 * ProcesoDTO procesoDTO = new ProcesoDTO();
	 * procesoDTO.setNumeroDocumento(numeroDocumento);
	 * 
	 * Request<ProcesoDTO> request = new Request<ProcesoDTO>(procesoDTO);
	 * 
	 * HttpHeaders headers = new HttpHeaders();
	 * 
	 * headers.setContentType(MediaType.APPLICATION_JSON); HttpEntity httpRequest =
	 * new HttpEntit	y<>(request, headers);
	 * 
	 * Response<DatosAspirante> response = restTemplate.postForObject(procesoUrl +
	 * "/proceso", httpRequest, Response.class);
	 * System.out.println("response is ------------->");
	 * System.out.println(response.getMessage());
	 * System.out.println(response.getPayload());
	 * 
	 * }
	 */

}
