package gov.mil.esp.pdf.rest.api.impl;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import gov.mil.esp.pdf.domain.logic.FormatFactory;
import gov.mil.esp.pdf.rest.api.Request;
import gov.mil.esp.pdf.rest.api.Response;
import gov.mil.esp.pdf.rest.api.dto.SolicitudFormato;


@RestController
@CrossOrigin(origins="*")
@RequestMapping(value = "/formato")
public class FormatoControllerImpl {
	
	

	@Autowired
	private FormatFactory formatFactory;	
	 
	public FormatoControllerImpl (FormatFactory formatFactory) {
		this.formatFactory = formatFactory;
	}
	
	@GetMapping("/{tipoFormato}/{documentoIdentidad}")
	public ResponseEntity<?> getFormatoAspirante(@PathVariable String tipoFormato, @PathVariable String documentoIdentidad) {

		String pdfBase64 = formatFactory.getFormat(
				tipoFormato, 
				documentoIdentidad).execute();
		
		Response<String> response =  new Response<String>();
		response.setHttpStatus(HttpStatus.OK);
		response.setPayload(pdfBase64);
		response.setMensaje("El pdf es:");

		
		return ResponseEntity.status(HttpStatus.OK).body(response);
		/*return ResponseEntity.status(HttpStatus.OK).body(formatFactory.getFormat(
				solicitudFormatoRequest
				.getMensaje()
				.getTipoFormato(), 
				solicitudFormatoRequest.getMensaje()
				.getNumeroDocumento()).execute());*/
	}
	
	
	
	public ResponseEntity<?> mockPdf(@RequestBody Request<SolicitudFormato> solicitudFormato) {
		System.out.println("in mock pdf");
		System.out.println(solicitudFormato);
		try {
			Resource resource = new ClassPathResource("formatoactualizacionejemplo.pdf");
			
			   InputStream in = resource.getInputStream();
			   
 
			   String encodedString = Base64.encodeBase64String(IOUtils.toByteArray(in));
			   
			//return ResponseEntity.status(HttpStatus.OK).body(IOUtils.toByteArray(in));
			
			return ResponseEntity.status(HttpStatus.OK).body(encodedString);
			
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		return null;
	}

}
