package gov.mil.esp.pdf.model.aspirante;

import lombok.Data;

@Data
public class BienPatrimonial {
	
	String tipo;
	String identificacion;
	String valor;
}