package gov.mil.esp.pdf.model.aspirante;

import lombok.Data;

@Data
public class AntecedenteJudicial {
    String fecha;
    String tipoInvestigacion;
    String causa;
    String autoridad;
    String estadoActualDelProceso;
    String esReponsable;
}