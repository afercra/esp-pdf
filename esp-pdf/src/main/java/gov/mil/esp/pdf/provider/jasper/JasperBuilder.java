package gov.mil.esp.pdf.provider.jasper;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import org.springframework.core.io.Resource;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JsonDataSource;

public interface JasperBuilder {

	Resource getJasperBaseFormatFileResource(String fileName);
	InputStream getInputStreamFromFileResource(Resource resource) throws IOException;
	JasperReport getJasperReportFromFileResourceInputStream(InputStream inputStream) throws JRException;
	JsonDataSource getJsonDataSourcefromStringifiedJsonObject(String StringifiedJsonObject) throws JRException;
	JasperPrint fillBaseFormatReportWithJsonData(JasperReport jasperReport, Map<String, Object> paramsMap, JsonDataSource jsonDataSource) throws JRException;
	byte [] getFinalFormatBytes(JasperPrint jasperPrint) throws JRException;
	public byte [] buildPdfFile(String fileName, String fileContent) throws IOException, JRException;
}
