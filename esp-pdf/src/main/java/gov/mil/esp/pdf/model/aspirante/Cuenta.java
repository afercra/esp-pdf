package gov.mil.esp.pdf.model.aspirante;

import lombok.Data;

@Data
public class Cuenta {

	String pais;
	String entidadFinanciera;
	String sedeCuenta;
	String tipoCuenta;
	String numeroCuenta;
	String saldoCuenta;

}
