package gov.mil.esp.pdf.rest.client.fallback;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;

import feign.FeignException;
import gov.mil.esp.pdf.model.aspirante.DatosPersona;
import gov.mil.esp.pdf.rest.api.Request;
import gov.mil.esp.pdf.rest.api.Response;
import gov.mil.esp.pdf.rest.client.PersonaClient;

public class PersonaClientFallback implements PersonaClient{

	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private Throwable cause;
	
	public PersonaClientFallback (Throwable cause) {
		this.cause = cause;
	}

	/*@Override
	public Response<DatosPersona> getDatosPersona(@RequestBody Request<String> usuario) {
	   if(cause instanceof FeignException && ((FeignException) cause).status() == 404) {
           logger.error("404 usuario: "
                   + usuario.getMensaje()+"no encontrado. Error message: "
                   + cause.getLocalizedMessage());
	   }else {
		   logger.error("Other error took place: " + cause.getLocalizedMessage());   
	   }
	return null;
		   
	}*/

	@Override
	public Response<DatosPersona> getDatosPersona(String documentoIdentidad) {
		// TODO Auto-generated method stub
		return null;
	}

}
