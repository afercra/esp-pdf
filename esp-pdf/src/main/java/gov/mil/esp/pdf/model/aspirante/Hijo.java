package gov.mil.esp.pdf.model.aspirante;

import lombok.Data;

@Data
public class Hijo {

	String fechaNacimiento;
	String nombreCompleto;
	String numeroDocumento;
	String primerApellido;
	String primerNombre;
	String segundoNombre;
	String tipoDocumento;
	String edad;

}