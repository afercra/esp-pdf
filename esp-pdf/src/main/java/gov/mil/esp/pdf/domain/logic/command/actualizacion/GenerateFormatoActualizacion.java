package gov.mil.esp.pdf.domain.logic.command.actualizacion;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import gov.mil.esp.pdf.domain.logic.command.GenerateFormato;
import gov.mil.esp.pdf.domain.logic.ingreso.FormatoIngresoBuilder;
import gov.mil.esp.pdf.domain.logic.template.FormatoGenericBuilderTemplate;
import gov.mil.esp.pdf.model.aspirante.DatosPersona;
import gov.mil.esp.pdf.model.formato.FormatoGenerico;
import gov.mil.esp.pdf.model.formato.ingreso.FormatoIngresoBase;
import net.sf.jasperreports.engine.JRException;

@Component
public class GenerateFormatoActualizacion extends FormatoGenericBuilderTemplate implements GenerateFormato {

	
	private FormatoIngresoBase formatoIngresoBase;
	
	
	private String documentoIdentidad;
	private final String formatName = "formatoIngreso.jasper";
	
	@Autowired
	private FormatoIngresoBuilder formatoIngresoBuilder;


	@Autowired
	public GenerateFormatoActualizacion() {
	}
	
	public GenerateFormatoActualizacion(String documentoIdentidad) {
		this.documentoIdentidad = documentoIdentidad;
	}
	
	public void setDocumentoIdentidad(String documentoIdentidad) {
		this.documentoIdentidad = documentoIdentidad;
	}
	
	public DatosPersona getDatosPersona() {
		return this.getDatosPersona(documentoIdentidad);
	}
	
	protected FormatoGenerico prepareFormat(String documentoIdentidad) {		
		return formatoIngresoBuilder.buildFormatoIngresoBase(getDatosPersona());
	}
		
	@Override
	protected String exportToPdfFile(FormatoGenerico formatoGenerico) throws IOException, JRException {		
		return this.generateBase64(formatName, formatoGenerico);
	}


	@Override
	public String execute() {
		try {
			return exportToPdfFile(prepareFormat(documentoIdentidad));
		} catch (IOException | JRException e) {
			System.out.println("error Getting file ---------------->>");
			e.printStackTrace();
		}
		return null;
	}



}
