package gov.mil.esp.pdf.provider.jasper;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JsonDataSource;
import net.sf.jasperreports.engine.util.JRLoader;

@Component
public class JasperBuilderImpl implements JasperBuilder {

	@Override
	public Resource getJasperBaseFormatFileResource(String fileName) {
		return new ClassPathResource(fileName);
	}

	@Override
	public InputStream getInputStreamFromFileResource(Resource resource) throws IOException {
		return resource.getInputStream();
	}

	@Override
	public JasperReport getJasperReportFromFileResourceInputStream(InputStream inputStream) throws JRException {
		return (JasperReport) JRLoader.loadObject(inputStream);
	}

	@Override
	public JsonDataSource getJsonDataSourcefromStringifiedJsonObject(String StringifiedJsonObject) throws JRException {
		
		return new JsonDataSource(new ByteArrayInputStream(StringifiedJsonObject.getBytes(StandardCharsets.UTF_8)));
	}

	@Override
	public JasperPrint fillBaseFormatReportWithJsonData(JasperReport jasperReport, Map<String, Object> paramsMap,
			JsonDataSource jsonDataSource) throws JRException {
		return JasperFillManager.fillReport(jasperReport, paramsMap, jsonDataSource);
	}
	
	@Override
	public byte[] getFinalFormatBytes(JasperPrint jasperPrint) throws JRException {
		
       return JasperExportManager.exportReportToPdf(jasperPrint);
	}
	
	@Override
	public byte [] buildPdfFile (String fileName, String fileContent) throws IOException, JRException {
		JsonDataSource jsonDataSource =
		getJsonDataSourcefromStringifiedJsonObject(fileContent);
		JasperReport jasperReport = getJasperReportFromFileResourceInputStream(getInputStreamFromFileResource(getJasperBaseFormatFileResource(fileName)));		
		HashMap<String, Object> params = new HashMap<String, Object>();
		JasperPrint jasperPrint = fillBaseFormatReportWithJsonData(jasperReport,params,jsonDataSource);
			
        byte [] filePdf = getFinalFormatBytes(jasperPrint); 
		//FileUtils.writeByteArrayToFile(new File("/home/usuario/Documentos/aaa.pdf"), filePdf);
		
		return filePdf;
	}


}
