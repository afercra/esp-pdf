package gov.mil.esp.pdf.model.aspirante;

import lombok.Data;

@Data
public class OrganismoEstatal {

	String entidad;
	String cargo;
	String motivoRetiro;
}
