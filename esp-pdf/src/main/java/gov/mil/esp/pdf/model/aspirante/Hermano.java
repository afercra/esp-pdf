package gov.mil.esp.pdf.model.aspirante;

import lombok.Data;

@Data
public class Hermano {

	String primerApellido;
	String segundoApellido;
	String primerNombre;
	String segundoNombre;
	String tipoDocumento;
	String numeroDocumento;
	String direccion;
	String departamento;
	String ciudad;
	String numeroCelular;
	String numeroTelefono;
	String ocupacion;
	String redesSociales;
	String nombreCompleto;
	String correoElectronico;
}