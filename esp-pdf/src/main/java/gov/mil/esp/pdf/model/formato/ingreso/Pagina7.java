package gov.mil.esp.pdf.model.formato.ingreso;

import gov.mil.esp.pdf.model.formato.PaginaFormato;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Pagina7 extends PaginaFormato {
	
	private String unidadRequiereEstudio; 
	private String dependenciaRequiereEstudio; 
	private String unidadSolicitaEstudio; 


}
