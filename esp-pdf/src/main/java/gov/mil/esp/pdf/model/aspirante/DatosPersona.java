package gov.mil.esp.pdf.model.aspirante;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class DatosPersona implements Serializable{
	
	
	private static final long serialVersionUID = 1L;
	
	@NotNull(message = "El campo dependencia solicitante no puede ser nulo.")
	@NotBlank(message = "El campo dependencia solicitante no puede estar vacio.")
	private String dependenciaSolicitante;
	
	@NotNull(message = "El campo cargo cargo Aspirante  no puede ser nulo.")
	@NotBlank(message = "El campo cargo cargoAspirante Solicitante no puede estar vacio.")	
	private String cargoAspirante;

	@NotNull(message="El campo primer apellido no puede ser nulo.")
	@NotBlank(message = "El campo primer apellido no puede estar vacio.")
	private String primerApellido;
	
	String segundoApellido;
	
	@NotNull(message="El campo primer Nombre es obligatorio.")
	@NotBlank(message = "El campo primer Nombre no puede estar vacio.")
	String primerNombre;
	
	String segundoNombre;
	
	@NotNull(message="El campo tipo documento de identidad es obligatorio.")
	@NotBlank(message = " El campo tipo documento de identidad no puede estar vacio ")
	String tipoDocumentoIdentidad;
	
	@NotNull(message="El campo número documento de identidad es obligatorio.")
	@NotBlank(message = "El campo número documento de identidad no puede estar vacio")
	String documentoIdentidad;
	
	@NotNull(message="El campo fecha expedición documento de identidad no puede ser nulo.")
	@NotBlank(message = "El campo fecha expedición documento de identidad no puede estar vacio.")
	String fechaExpedicionDocumentoIdentidad;
	
	@NotNull(message="El campo departamento expedición documento de identidad no puede ser nulo.")
	@NotBlank(message = "El campo departamento expedición documento de identidad no puede estar vacio.")
	String departamentoExpedicionDocumentoIdentidad;
	
	@NotNull(message="El campo ciudad expedición no puede ser nulo.")
	@NotBlank(message = "El campo ciudad expedición documento identidad no puede estar vacio.")
	String ciudadExpedicionDocumentoIdentidad;

	@NotNull(message="El campo pais expedición documento de identidad no puede ser nulo.")
	@NotBlank(message = "El campo pais expedición documento de identidad no puede estar vacio.")
	String paisExpedicionDocumentoIdentidad;
	
	String numeroPasaporte;
	String departamentoExpedicionPasaporte;
	String ciudadExpedicionPasaporte;
	String numeroLibretaMilitar;
	String claseLibretaMilitar;
	String fechaExpedicionLibretaMilitar;
	String distritoLibretaMilitar;
	
	@NotNull(message="El campo pais nacimiento no puede ser nulo.")
	@NotBlank(message = "El campo pais nacimiento no puede estar vacio.")
	String paisNacimiento;
	
	@NotNull(message="El campo departamento nacimiento no puede ser nulo.")
	@NotBlank(message = "El campo departamento nacimiento no puede estar vacio.")
	String departamentoNacimiento;
	
	@NotNull(message="El campo ciudad nacimiento no puede ser nulo.")
	@NotBlank(message = "El campo ciudad nacimiento no puede estar vacio.")
	String ciudadNacimiento;
	
	@NotNull(message="El campo fecha de nacimiento no puede ser nulo.")
	@NotBlank(message = "El campo fecha de nacimiento no puede estar vacio.")
	String fechaNacimiento;
	
	@NotNull(message="El campo dirección residecia actual no puede ser nulo.")
	@NotBlank(message = "El campo dirección residecia actual de identidad no puede estar vacio.")
	String direccionResidenciaActual;
	

	String numeroCelularResidenciaActual;
	
	String numeroTelefonoResidenciaActual;
	
	@NotNull(message="El campo pais residencia actual no puede ser nulo.")
	@NotBlank(message = "El campo pais residencia actual no puede estar vacio.")
	String paisResidenciaActual;
	
	@NotNull(message="El campo pais expedición documento de identidad no puede ser nulo.")
	@NotBlank(message = "El campo pais expedición documento de identidad no puede estar vacio.")
	String departamentoResidenciaActual;
	
	@NotNull(message="El campo ciudad de residencia actual no puede ser nulo.")
	@NotBlank(message = "El campo ciudad de residencia actual no puede estar vacio.")
	String ciudadResidenciaActual;
	
	@NotNull(message="El campo ciudad de residencia actual no puede ser nulo.")
	@NotBlank(message = "El campo ciudad de residencia actual no puede estar vacio.")
	String barrioResidenciaActual;
	
	String direccionResidenciaAnterior;
	
	String numeroCelularResidenciaAnterior;
	
	String numeroTelefonoResidenciaAnterior;
	String paisResidenciaAnterior;
	String departamentoResidenciaAnterior;
	String ciudadResidenciaAnterior;

	String barrioResidenciaAnterior;
	
	@NotNull(message="El campo estado civil no puede ser nulo.")
	@NotBlank(message = "El campo estado civil no puede estar vacio.")
	String estadoCivil;
	
	@NotNull(message="El campo grupo sanguineo no puede ser nulo.")
	@NotBlank(message = "El campo  grupo sanguineo no puede estar vacio.")
	String grupoSanguineo;
	
	String profesion;
	
	String tarjetaProfesional;
	
	@NotNull(message="El campo estatura no puede ser nulo.")
	@NotBlank(message = "El campo estatura no puede estar vacio.")
	String estatura;
	
	@NotNull(message="El campo peso no puede ser nulo.")
	@NotBlank(message = "El campo peso no puede estar vacio.")
	String peso;
	
	
	String correoElectronico;	
	
	String nombreCompletoCompaneroSentimental;
	String primerApellidoCompaneroSentimental;
	String segundoApellidoCompaneroSentimental;
	String primerNombreCompaneroSentimental;
	String segundoNombreCompaneroSentimental;
	String tipoDocumentoCompaneroSentimental;
	String numeroDocumentoCompaneroSentimental;
	String profesionUOficionCompaneroSentimental;
	String direccionCompaneroSentimental;
	String departamentoCompaneroSentimiental;
	String ciudadCompaneroSentimental;
	String numeroCelularCompaneroSentimental;
	String numeroTelefonoCompaneroSentimental;
	String correoElectronicoCompaneroSentimental;
	String primerApellidoPadre;
	String segundoApellidoPadre;
	String primerNombrePadre;
	String segundoNombrePadre;
	String vivePadre;
	String tipoDocumentoPadre;
	String numeroDocumentoPadre;
	String direccionPadre;
	String departamentoPadre;
	String ciudadPadre;
	String numeroCelularPadre;
	String numeroTelefonoPadre;
	String ocupacionPadre;
	String primerApellidoMadre;
	String segundoApellidoMadre;
	String primerNombreMadre;
	String segundoNombreMadre;
	String viveMadre;
	String tipoDocumentoMadre;
	String numeroDocumentoMadre;
	String direccionMadre;
	String departamentoMadre;
	String ciudadMadre;
	String numeroCelularMadre;
	String numeroTelefonoMadre;
	String ocupacionMadre;
	String salarioIngresosLaborales;
	String cesantiasIntereses;
	String arriendos;
	String honorarios;
	String actividadesEnElexterior;
	String armasQueConoce;
	String fechaConocimientoArmas;
	String armasQueHaStringpualado;
	String fechaYLugarManipulacionArmas;
	String opinionSobreOrganizacionesIlegales;
	String tieneInformacionRiesgosaParaNacion;
	String informacionAmenazanteParaNacion;
	String esRecomendadoporAlguienDeLaInstitucion;
	String tieneInformacionActosCorrupcionEnInstitucion;
	String informacionActosCorrupcionEnInstitucion;
	String empleosugeridoPorPrimerApellido;
	String empleosugeridoPorSegundoApellido;
	String empleosugeridoPorPrimerNombre;
	String empleosugeridoPorSegundoNombre;
	String empleoSugeridoPorNombreCompleto;
	String empleosugeridoPorDireccion;
	String empleosugeridoPorTelefono;
	String razonDeBuscarVinculoConlaInstitucionYExpectativas;
	String datosAdicionales;
	String nacionalidad;
	String redSocialConyugue;
	
	String opinionGruposMarginales;
	String conoceArmasDeFuegoExplosivosCuales;
	String conoceArmasDeFuegoExplosivosCuandoYDonde;
	String conoceArmasDeFuegoExplosivosCuando;
	String conoceArmasDeFuegoExplosivosCualesHaManipulado;
	String armasManipuladasEnCompaniaDe;
	String recomendadoPor;
	String residenciaAnteriorDesde;
	String residenciaAnteriorHasta;
	private String redesSociales;
	
	List<Hijo> hijos;
	List<Hermano> hermanos;
	List<EstudioRealizado> estudiosRealizados;
	List<Idioma> idiomas;
	List<Empresa> empresas;
	List<Referencia> referenciasNoFamiliares;
	List<Referencia> parientesOAmigosEnLaInstitucion;
	List<OtroIngreso> otrosIngresos;
	List<Cuenta> cuentasDinero;
	List<BienPatrimonial> bienesPatrimoniales;
	List<ObligacionCrediticia> obligacionesCrediticias;
	List<ParticipacionOrganizacion> participacionOrganizaciones;
	List<ActividadEconomicaPrivada> actividadEconomicaPrivada;
	List<ViajeExterior> viajesAlExterior;
	List<OrganismoEstatal> organizacionesGubarnamentales;
	List<AntecedenteJudicial> antecedentesJudiciales;
	List<Referencia> contactoEmergencia;

	byte [] imagenAspirante;
	byte [] imagenPerfilDerechoAspirante;
	byte [] imagenPerfilIzquierdoAspirante;
	byte [] imagenViviendaAspirante;
}