package gov.mil.esp.pdf.model.formato.ingreso;

import gov.mil.esp.pdf.model.formato.PaginaFormato;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Pagina1 extends PaginaFormato {

	private String dependenciaSolicitante;
	private String empleCargoAspira;
	private String primerNombre; 
	private String lugaryfecha; 
	private String grado; 
	private String cargoActual; 
	private String profesion; 
	private String primerApellido; 
	private String segundoApellido; 
	private String segundoNombre; 
	private String tipoDocumento; 
	private String numeroDocumento;
	private String fechaYLugarExpedicion; 
	private String pasaporte; 
	private String lugarExpedicionDocumento; 
	private String lugarNacimiento; 
	private String diaNacimiento; 
	private String mesNacimiento; 
	private String anoNacimiento; 
	private String estadoCivil; 
	private String gsrh; 
	private String profesionUOficio; 
	private String tarjetaProfesional; 
	private String nacionalidad; 
	private String estatura; 
	private String peso; 

}
