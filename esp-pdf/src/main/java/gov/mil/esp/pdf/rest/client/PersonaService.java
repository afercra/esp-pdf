package gov.mil.esp.pdf.rest.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import gov.mil.esp.pdf.model.aspirante.DatosPersona;
import gov.mil.esp.pdf.rest.api.Request;
import gov.mil.esp.pdf.rest.api.Response;

@Component
public class PersonaService {

	
	private PersonaClient personaClient;
	
	@Autowired
	public PersonaService (PersonaClient personaClient) {
		this.personaClient = personaClient;
	}
	
	public DatosPersona getDatosPersona(String documentoIdentidad ) {
		System.out.println("In get datosPersona");
		//return buildPersonaResponse(personaClient.getDatosPersona(buildPersonaRequest(documentoIdentidad))); 
		return buildPersonaResponse(personaClient.getDatosPersona(documentoIdentidad));
	}
	
	private Request<String> buildPersonaRequest(String documentoIdentidad) {
		System.out.println("in build ṕersona");
		System.out.println(documentoIdentidad);
		return new Request(documentoIdentidad);
		
	}
	
	private DatosPersona buildPersonaResponse(Response<DatosPersona> requestDatosPersona) {		
		System.out.println("in build persona response ");
		System.out.println(requestDatosPersona);
		return requestDatosPersona.getPayload();
	}
}
