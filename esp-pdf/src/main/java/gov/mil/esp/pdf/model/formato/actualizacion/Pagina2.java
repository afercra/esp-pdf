package gov.mil.esp.pdf.model.formato.actualizacion;

import gov.mil.esp.pdf.model.formato.PaginaFormato;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Pagina2 extends PaginaFormato {

	private String barrioResidencia;
	private String telefonoCelularResidencia;
	private String telefonoFijoResidencia;
	private String ciudadYdepartamentoResidencia;
	private String correosElectronicosResidencia;
	private String redesSocialesResidencia;
	private String residenciaAnteriorDesde;
	private String residenciaAnteriorHasta;
	private String residenciaAnteriorDireccion;
	private String telefonosResidenciaAnterior;
	private String ciudadPaisResidenciaAnterior;
	private String nombreEsposa;
	private String profesionEsposa;
	private String numeroDocumentoEsposa;
	private String direccionEsposa;
	private String celularYTelefonoEsposa;
	private String correoElectronicoEsposa;
	private String redesSocialesEsposa;
	private String nombreHijo1;
	private String nombreHijo2;
	private String nombreHijo3;
	private String nombreHijo4;
	private String numeroIdentificacionHijo1;
	private String numeroIdentificacionHijo2;
	private String numeroIdentificacionHijo3;
	private String numeroIdentificacionHijo4;
	private String nombrePadre;
	private String padreViveSi;
	private String padreViveNo;
	private String numeroDocumentoPadre;
	private String telefonosPadre;
	private String direccionPadre;
	private String profesionPadre;
	private String nombreMadre;
	private String madreViveSi;
	private String madreViveNo;
	private String numeroDocumentoMadre;
	private String telefonosMadre;
	private String direccionMadre;
	private String profesionMadre;
	private String edadHijo1;
	private String edadHijo2;
	private String edadHijo3;
	private String edadHijo4;

}
