package gov.mil.esp.pdf.domain.logic.command.actualizacion.pagina;

import org.springframework.stereotype.Component;

import gov.mil.esp.pdf.domain.logic.command.GeneratePage;
import gov.mil.esp.pdf.model.aspirante.DatosPersona;
import gov.mil.esp.pdf.model.formato.PaginaFormato;
import gov.mil.esp.pdf.model.formato.ingreso.Pagina4;

@Component
public class GeneratePaginaActualizacion4 implements GeneratePage{

	@Override
	public PaginaFormato execute(DatosPersona datosPersona) {
		return Pagina4.builder()
				

				

				
				.nombresApellidosReferencia1("")
				.ocupacionReferencia1("")
				.direccionReferencia1("")
				.telefonosReferencia1("")
				.nombresApellidosReferencia2("")
				.ocupacionReferencia2("")
				.direccionReferencia2("")
				.telefonosReferencia2("")
				
				.organismoConocidoEntidadPublica2("")
				.unidadConocidoEntidadPublica2("")
				.telefonosConocidoEntidadPublica2("")
				.nombresConocidoEntidadPublica2("")
				.direccionConocidoEntidadPublica2("")
				.gradoConocidoEntidadPublica2("")
				.telefonosConocidoEntidadPublica1("")
				.unidadConocidoEntidadPublica1("")
				.organismoConocidoEntidadPublica1("")
				.direccionConocidoEntidadPublica1("")
				.nombresConocidoEntidadPublica1("")
				.gradoConocidoEntidadPublica1("")
				
	             
				.otrosIngresos("")
				.honorarios("")
				.arriendos("")

				.salariosDemasIngresos("")
				.cesantiaseintereses("")
				
				

				.saldoCuenta1("")
				.numeroCuenta1("")
				.tipoCuenta1("")
				.sedeFinancieraCuenta1("")
				.entidadFinancieraCuenta1("")
				.entidadFinancieraCuenta2("")
				.sedeFinancieraCuenta2("")
				.tipoCuenta2("")
				.numeroCuenta2("")
				.saldoCuenta2("")
				.entidadFinancieraCuenta3("")
				.sedeFinancieraCuenta3("")
				.tipoCuenta3("")
				.numeroCuenta3("")
				.saldoCuenta3("")

				
				.tipoBien1("")
				.identificacionBien1("")
				.valorBien1("")
				.tipoBien2("")
				.identificacionBien2("")
				.valorBien2("")
				.tipoBien3("")
				.identificacionBien3("")
				.valorBien3("")
				.tipoBien4("")
				.identificacionBien4("")
				.valorBien4("")
				
			

				
				.entidadObligacionCrediticia1("")
				.valorObligacionCrediticia1("")
				.conceptoObligacionCrediticia1("")
				.entidadObligacionCrediticia2("")
				.valorObligacionCrediticia2("")
				.conceptoObligacionCrediticia2("")
				.entidadObligacionCrediticia3("")
				.valorObligacionCrediticia3("")
				.conceptoObligacionCrediticia3("")
				.entidadObligacionCrediticia4("")
				.valorObligacionCrediticia4("")
				.conceptoObligacionCrediticia4("")
				
				.nombreOrganizacion1("")
				.calidadMiembroOrganizacion1("")
				.nombreOrganizacion2("")
				.calidadMiembroOrganizacion2("")




				
					.build();
	}
	
	
}