package gov.mil.esp.pdf.domain.logic.command.actualizacion.pagina;

import org.springframework.stereotype.Component;

import gov.mil.esp.pdf.domain.logic.command.GeneratePage;
import gov.mil.esp.pdf.model.aspirante.DatosPersona;
import gov.mil.esp.pdf.model.formato.PaginaFormato;
import gov.mil.esp.pdf.model.formato.actualizacion.Pagina6;

@Component
public class GeneratePaginaActualizacion6 implements GeneratePage{

	@Override
	public PaginaFormato execute(DatosPersona datosPersona) {

		return Pagina6.builder()/*.razonesVinculaciounInstitucion("").fechaAntecedenteJudicial1("")
				.tipoAntecedenteJudicial1("").causaAntecedenteJudicial1("").estadoActualAntecedente1("")
				.responsableAntecedenteJudicial1SI("").responsableAntecedenteJudicial1NO("")
				.fechaAntecedenteJudicial2("").tipoAntecedenteJudicial2("").causaAntecedenteJudicial2("")
				.estadoActualAntecedente2("").responsableAntecedenteJudicial2SI("")
				.responsableAntecedenteJudicial2NO("").autoridadAntecedenteJudicial1("")
				.autoridadAntecedenteJudicial2("").datosAdicionales("")*/.build();
	}
	
	
}