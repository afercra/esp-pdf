package gov.mil.esp.pdf.model.aspirante;

import lombok.Data;

@Data
public class EstudioRealizado {
	
	String estudioRealizado;
	String anioFinalizacion;
	String tituloObtenido;
	String nombreInstitucion;
	String paisInstitucion;
	String departamentoInstitucion;
	String ciudad;

}
