package gov.mil.esp.pdf.rest.api;

public interface FormatoController {
	
	void getFormatoAspirante(String numeroDocumento);
	void getFormatoRequerimiento(String numeroDocumento);
	void getFormatoAceptacion(String numeroDocumento);
	
}
