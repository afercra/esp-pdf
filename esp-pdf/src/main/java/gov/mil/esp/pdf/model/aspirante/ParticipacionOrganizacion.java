package gov.mil.esp.pdf.model.aspirante;

import lombok.Data;

@Data
public class ParticipacionOrganizacion {
	
	String nombre;
	String calidadMiembro;

}